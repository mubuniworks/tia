<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKeyRatiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('key_ratios', function (Blueprint $table) {
            $table->id();
            $table->text('tier')->nullable();

            //revenue growth
            $table->text('py3_revenue_growth');
            $table->text('py2_revenue_growth');
            $table->text('py1_revenue_growth');
            $table->text('ytd_revenue_growth');
            $table->text('fp1_revenue_growth');
            $table->text('fp2_revenue_growth');
            $table->text('fp3_revenue_growth');
            $table->text('fp4_revenue_growth');
            $table->text('fp5_revenue_growth');

            //gp margin
            $table->text('py3_gp_margin');
            $table->text('py2_gp_margin');
            $table->text('py1_gp_margin');
            $table->text('ytd_gp_margin');
            $table->text('fp1_gp_margin');
            $table->text('fp2_gp_margin');
            $table->text('fp3_gp_margin');
            $table->text('fp4_gp_margin');
            $table->text('fp5_gp_margin');

            //pbt margin
            $table->text('py3_pbt_margin');
            $table->text('py2_pbt_margin');
            $table->text('py1_pbt_margin');
            $table->text('ytd_pbt_margin');
            $table->text('fp1_pbt_margin');
            $table->text('fp2_pbt_margin');
            $table->text('fp3_pbt_margin');
            $table->text('fp4_pbt_margin');
            $table->text('fp5_pbt_margin');

            //ebitda key
            $table->text('py3_ebitda');
            $table->text('py2_ebitda');
            $table->text('py1_ebitda');
            $table->text('ytd_ebitda');
            $table->text('fp1_ebitda');
            $table->text('fp2_ebitda');
            $table->text('fp3_ebitda');
            $table->text('fp4_ebitda');
            $table->text('fp5_ebitda');

            //ebitda margin
            $table->text('py3_ebitda_margin');
            $table->text('py2_ebitda_margin');
            $table->text('py1_ebitda_margin');
            $table->text('ytd_ebitda_margin');
            $table->text('fp1_ebitda_margin');
            $table->text('fp2_ebitda_margin');
            $table->text('fp3_ebitda_margin');
            $table->text('fp4_ebitda_margin');
            $table->text('fp5_ebitda_margin');

            //pat margin
            $table->text('py3_pat_margin');
            $table->text('py2_pat_margin');
            $table->text('py1_pat_margin');
            $table->text('ytd_pat_margin');
            $table->text('fp1_pat_margin');
            $table->text('fp2_pat_margin');
            $table->text('fp3_pat_margin');
            $table->text('fp4_pat_margin');
            $table->text('fp5_pat_margin');

            //tnav key
            $table->text('py3_tnav');
            $table->text('py2_tnav');
            $table->text('py1_tnav');
            $table->text('ytd_tnav');
            $table->text('fp1_tnav');
            $table->text('fp2_tnav');
            $table->text('fp3_tnav');
            $table->text('fp4_tnav');
            $table->text('fp5_tnav');

            //tnav key
            $table->text('py3_current_ratio');
            $table->text('py2_current_ratio');
            $table->text('py1_current_ratio');
            $table->text('ytd_current_ratio');
            $table->text('fp1_current_ratio');
            $table->text('fp2_current_ratio');
            $table->text('fp3_current_ratio');
            $table->text('fp4_current_ratio');
            $table->text('fp5_current_ratio');

            //tnav key
            $table->text('py3_debt_ratio');
            $table->text('py2_debt_ratio');
            $table->text('py1_debt_ratio');
            $table->text('ytd_debt_ratio');
            $table->text('fp1_debt_ratio');
            $table->text('fp2_debt_ratio');
            $table->text('fp3_debt_ratio');
            $table->text('fp4_debt_ratio');
            $table->text('fp5_debt_ratio');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('key_ratios');
    }
}
