<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBackgroundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('backgrounds', function (Blueprint $table) {
            $table->id();
            $table->string('shareholder_name')->nullable();
            $table->string('shareholder_id')->nullable();
            $table->string('entity_name')->nullable();
            $table->string('entity_registration_number')->nullable();
            $table->string('business_status'); //0 is a startup 1 is an existing business
            $table->string('individual_shareholder_percentage')->nullable();
            $table->string('entity_shareholder_percentage')->nullable();
            $table->string('management_company_name')->nullable();
            $table->string('management_registration_number')->nullable();
            $table->string('management_company_position')->nullable();
            $table->string('permanent_staff_no')->nullable();
            $table->string('temporary_staff_no')->nullable();
            $table->string('business_location_province');
            $table->string('business_nearest_city_location');
            $table->string('project_actual_location_area');
            $table->string('business_description')->nullable();
            $table->string('business_goto_strategy')->nullable();
            $table->string('business_competitive_advantage')->nullable();
            $table->string('business_areas_of_improvement')->nullable();
            $table->string('business_strategic_opportunities')->nullable();
            $table->string('business_financial_year_end')->nullable();
            $table->string('business_accountant_name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('backgrounds');
    }
}
