<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Maatwebsite\Excel\Concerns\Exportable;

class Refiner extends Model
{
    use Notifiable, Exportable;
}
