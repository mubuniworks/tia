<?php

namespace App\Exports;

use App\Background;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class BackgroundsExport implements FromQuery, ShouldAutoSize, WithTitle, WithHeadings, WithStyles
{
    public function headings(): array
    {
        return [
            'ID', 'Shareholder Name', 'Shareholder ID', 'Entity Name', 'Entity Registration Number',
            'Business Status (0 is a startup 1 is an existing business)', 'Individual Shareholder Percentage',
            'Entity Shareholder Percentage', 'Management Company Name', 'Management Registration Number',
            'Management Company Position', 'Permanent Staff No', 'Temporary Staff No', 'Business Location Province',
            'Business Nearest City Location', 'Project Actual Location Area', 'Business Description',
            'Business Goto Strategy', 'Business Competitive Advantage', 'Business Areas Of Improvement',
            'Business Strategic Opportunities', 'Business Financial Year End', 'Business Accountant Name',
        ];
    }

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Company Background';
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
        ];
    }


    /**
     * @return Builder
     */
    public function query()
    {
        return Background::query()
            ->where('id', $this->id);
    }
}
