<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ReportsExport implements WithMultipleSheets, ShouldAutoSize
{
    use Exportable;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [
            new ApplicantsExport($this->id),
            new ComplianceExport($this->id),
            new BackgroundsExport($this->id),
            new FinancialExport($this->id),
            new KeyRatiosExport($this->id),
        ];

        return $sheets;
    }
}
