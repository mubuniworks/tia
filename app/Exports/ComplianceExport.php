<?php

namespace App\Exports;

use App\Compliance;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ComplianceExport implements FromQuery, ShouldAutoSize, WithTitle, WithHeadings, WithStyles
{
    public function headings(): array
    {
        return [
            'ID', 'Company Name', 'Registration No', 'Tax Registration No', 'Vat Registration No', 'Contact Person',
            'Contact Person_email', 'Contact Person_mobile',
        ];
    }

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Builder
     */
    /**
     * @return string
     */
    public function title(): string
    {
        return 'Compliance Details';
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
        ];
    }


    public function query()
    {
        return Compliance::query()
            ->where('id', $this->id);
    }
}
