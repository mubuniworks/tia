<?php

namespace App\Exports;

use App\Applicant;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ApplicantsExport implements FromQuery, ShouldAutoSize, WithTitle, WithHeadings, WithStyles
{
    public function headings(): array
    {
        return [
            'ID', 'Firstname', 'Lastname', 'Race', 'Nationality', 'Email', 'Phone',
            'Business Status (0 - startup, 1 - existing business)',
        ];
    }

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Applicant Details';
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
        ];
    }


    /**
     * @return Builder
     */
    public function query()
    {
        return Applicant::query()
            ->where('id', $this->id);
    }
}
