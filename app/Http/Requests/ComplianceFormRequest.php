<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ComplianceFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name'  => ['required', 'string', 'max:255'],
            'registration_no'  => ['required', 'string', 'max:255'],
            'tax_registration_no'  => ['required', 'string', 'max:255'],
            'vat_registration_no'  => ['required', 'string', 'max:255'],
            'contact_person'  => ['required', 'string', 'max:255'],
            'contact_person_email'  => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'contact_person_mobile'  => ['required', 'string', 'max:255'],
        ];
    }
}
