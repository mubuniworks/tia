<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FinancialFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'is_revenue_py3' => ['required', 'number', 'max:10'], 'is_revenue_py2' => ['required', 'number', 'max:10'], 'is_revenue_py1' => ['required', 'number', 'max:10'],
            'is_revenue_ytd' => ['required', 'number', 'max:10'], 'is_revenue_fp5' => ['required', 'number', 'max:10'], 'is_revenue_fp4' => ['required', 'number', 'max:10'],
            'is_revenue_fp3' => ['required', 'number', 'max:10'], 'is_revenue_fp2' => ['required', 'number', 'max:10'], 'is_revenue_fp1' => ['required', 'number', 'max:10'],
            'is_cost_of_sales_py3' => ['required', 'number', 'max:10'], 'is_cost_of_sales_py2' => ['required', 'number', 'max:10'],
            'is_cost_of_sales_py1' => ['required', 'number', 'max:10'], 'is_cost_of_sales_ytd' => ['required', 'number', 'max:10'],
            'is_cost_of_sales_fp5' => ['required', 'number', 'max:10'], 'is_cost_of_sales_fp4' => ['required', 'number', 'max:10'],
            'is_cost_of_sales_fp3' => ['required', 'number', 'max:10'], 'is_cost_of_sales_fp2' => ['required', 'number', 'max:10'],
            'is_cost_of_sales_fp1' => ['required', 'number', 'max:10'], 'is_gross_profit_py3' => ['required', 'number', 'max:10'],
            'is_gross_profit_py2' => ['required', 'number', 'max:10'], 'is_gross_profit_py1' => ['required', 'number', 'max:10'],
            'is_gross_profit_ytd' => ['required', 'number', 'max:10'], 'is_gross_profit_fp5' => ['required', 'number', 'max:10'],
            'is_gross_profit_fp4' => ['required', 'number', 'max:10'], 'is_gross_profit_fp3' => ['required', 'number', 'max:10'],
            'is_gross_profit_fp2' => ['required', 'number', 'max:10'], 'is_gross_profit_fp1' => ['required', 'number', 'max:10'],
            'is_operating_expenses_py3' => ['required', 'number', 'max:10'], 'is_operating_expenses_py2' => ['required', 'number', 'max:10'],
            'is_operating_expenses_py1' => ['required', 'number', 'max:10'], 'is_operating_expenses_ytd' => ['required', 'number', 'max:10'],
            'is_operating_expenses_fp5' => ['required', 'number', 'max:10'], 'is_operating_expenses_fp4' => ['required', 'number', 'max:10'],
            'is_operating_expenses_fp3' => ['required', 'number', 'max:10'], 'is_operating_expenses_fp2' => ['required', 'number', 'max:10'],
            'is_operating_expenses_fp1' => ['required', 'number', 'max:10'], 'is_profit_before_tax_py3' => ['required', 'number', 'max:10'],
            'is_profit_before_tax_py2' => ['required', 'number', 'max:10'], 'is_profit_before_tax_py1' => ['required', 'number', 'max:10'],
            'is_profit_before_tax_ytd' => ['required', 'number', 'max:10'], 'is_profit_before_tax_fp5' => ['required', 'number', 'max:10'],
            'is_profit_before_tax_fp4' => ['required', 'number', 'max:10'], 'is_profit_before_tax_fp3' => ['required', 'number', 'max:10'],
            'is_profit_before_tax_fp2' => ['required', 'number', 'max:10'], 'is_profit_before_tax_fp1' => ['required', 'number', 'max:10'],
            'is_tax_py3' => ['required', 'number', 'max:10'], 'is_tax_py2' => ['required', 'number', 'max:10'], 'is_tax_py1' => ['required', 'number', 'max:10'],
            'is_tax_ytd' => ['required', 'number', 'max:10'], 'is_tax_fp5' => ['required', 'number', 'max:10'], 'is_tax_fp4' => ['required', 'number', 'max:10'],
            'is_tax_fp3' => ['required', 'number', 'max:10'], 'is_tax_fp2' => ['required', 'number', 'max:10'], 'is_tax_fp1' => ['required', 'number', 'max:10'],
            'is_profit_after_tax_py3' => ['required', 'number', 'max:10'], 'is_profit_after_tax_py2' => ['required', 'number', 'max:10'],
            'is_profit_after_tax_py1' => ['required', 'number', 'max:10'], 'is_profit_after_tax_ytd' => ['required', 'number', 'max:10'],
            'is_profit_after_tax_fp5' => ['required', 'number', 'max:10'], 'is_profit_after_tax_fp4' => ['required', 'number', 'max:10'],
            'is_profit_after_tax_fp3' => ['required', 'number', 'max:10'], 'is_profit_after_tax_fp2' => ['required', 'number', 'max:10'],
            'is_profit_after_tax_fp1' => ['required', 'number', 'max:10'], 'is_depreciation_py3' => ['required', 'number', 'max:10'],
            'is_depreciation_py2' => ['required', 'number', 'max:10'], 'is_depreciation_py1' => ['required', 'number', 'max:10'],
            'is_depreciation_ytd' => ['required', 'number', 'max:10'], 'is_depreciation_fp5' => ['required', 'number', 'max:10'],
            'is_depreciation_fp4' => ['required', 'number', 'max:10'], 'is_depreciation_fp3' => ['required', 'number', 'max:10'],
            'is_depreciation_fp2' => ['required', 'number', 'max:10'], 'is_depreciation_fp1' => ['required', 'number', 'max:10'],
            'is_interest_expense_py3' => ['required', 'number', 'max:10'], 'is_interest_expense_py2' => ['required', 'number', 'max:10'],
            'is_interest_expense_py1' => ['required', 'number', 'max:10'], 'is_interest_expense_ytd' => ['required', 'number', 'max:10'],
            'is_interest_expense_fp5' => ['required', 'number', 'max:10'], 'is_interest_expense_fp4' => ['required', 'number', 'max:10'],
            'is_interest_expense_fp3' => ['required', 'number', 'max:10'], 'is_interest_expense_fp2' => ['required', 'number', 'max:10'],
            'is_interest_expense_fp1' => ['required', 'number', 'max:10'], 'bs_total_assets_py3' => ['required', 'number', 'max:10'],
            'bs_total_assets_py2' => ['required', 'number', 'max:10'], 'bs_total_assets_py1' => ['required', 'number', 'max:10'],
            'bs_total_assets_ytd' => ['required', 'number', 'max:10'], 'bs_total_assets_fp5' => ['required', 'number', 'max:10'],
            'bs_total_assets_fp4' => ['required', 'number', 'max:10'], 'bs_total_assets_fp3' => ['required', 'number', 'max:10'],
            'bs_total_assets_fp2' => ['required', 'number', 'max:10'], 'bs_total_assets_fp1' => ['required', 'number', 'max:10'],
            'bs_fixed_assets_py3' => ['required', 'number', 'max:10'], 'bs_fixed_assets_py2' => ['required', 'number', 'max:10'],
            'bs_fixed_assets_py1' => ['required', 'number', 'max:10'], 'bs_fixed_assets_ytd' => ['required', 'number', 'max:10'],
            'bs_fixed_assets_fp5' => ['required', 'number', 'max:10'], 'bs_fixed_assets_fp4' => ['required', 'number', 'max:10'],
            'bs_fixed_assets_fp3' => ['required', 'number', 'max:10'], 'bs_fixed_assets_fp2' => ['required', 'number', 'max:10'],
            'bs_fixed_assets_fp1' => ['required', 'number', 'max:10'], 'bs_current_assets_py3' => ['required', 'number', 'max:10'],
            'bs_current_assets_py2' => ['required', 'number', 'max:10'], 'bs_current_assets_py1' => ['required', 'number', 'max:10'],
            'bs_current_assets_ytd' => ['required', 'number', 'max:10'], 'bs_current_assets_fp5' => ['required', 'number', 'max:10'],
            'bs_current_assets_fp4' => ['required', 'number', 'max:10'], 'bs_current_assets_fp3' => ['required', 'number', 'max:10'],
            'bs_current_assets_fp2' => ['required', 'number', 'max:10'], 'bs_current_assets_fp1' => ['required', 'number', 'max:10'],
            'bs_total_liabilities_py3' => ['required', 'number', 'max:10'], 'bs_total_liabilities_py2' => ['required', 'number', 'max:10'],
            'bs_total_liabilities_py1' => ['required', 'number', 'max:10'], 'bs_total_liabilities_ytd' => ['required', 'number', 'max:10'],
            'bs_total_liabilities_fp5' => ['required', 'number', 'max:10'], 'bs_total_liabilities_fp4' => ['required', 'number', 'max:10'],
            'bs_total_liabilities_fp3' => ['required', 'number', 'max:10'], 'bs_total_liabilities_fp2' => ['required', 'number', 'max:10'],
            'bs_total_liabilities_fp1' => ['required', 'number', 'max:10'], 'bs_longterm_liabilities_py3' => ['required', 'number', 'max:10'],
            'bs_longterm_liabilities_py2' => ['required', 'number', 'max:10'], 'bs_longterm_liabilities_py1' => ['required', 'number', 'max:10'],
            'bs_longterm_liabilities_ytd' => ['required', 'number', 'max:10'], 'bs_longterm_liabilities_fp5' => ['required', 'number', 'max:10'],
            'bs_longterm_liabilities_fp4' => ['required', 'number', 'max:10'], 'bs_longterm_liabilities_fp3' => ['required', 'number', 'max:10'],
            'bs_longterm_liabilities_fp2' => ['required', 'number', 'max:10'], 'bs_longterm_liabilities_fp1' => ['required', 'number', 'max:10'],
            'bs_current_liabilities_py3' => ['required', 'number', 'max:10'], 'bs_current_liabilities_py2' => ['required', 'number', 'max:10'],
            'bs_current_liabilities_py1' => ['required', 'number', 'max:10'], 'bs_current_liabilities_ytd' => ['required', 'number', 'max:10'],
            'bs_current_liabilities_fp5' => ['required', 'number', 'max:10'], 'bs_current_liabilities_fp4' => ['required', 'number', 'max:10'],
            'bs_current_liabilities_fp3' => ['required', 'number', 'max:10'], 'bs_current_liabilities_fp2' => ['required', 'number', 'max:10'],
            'bs_current_liabilities_fp1' => ['required', 'number', 'max:10'], 'bs_equity_py3' => ['required', 'number', 'max:10'],
            'bs_equity_py2' => ['required', 'number', 'max:10'], 'bs_equity_py1' => ['required', 'number', 'max:10'], 'bs_equity_ytd' =>
            ['required', 'number', 'max:10'], 'bs_equity_fp5' => ['required', 'number', 'max:10'], 'bs_equity_fp4' => ['required', 'number', 'max:10'],
            'bs_equity_fp3' => ['required', 'number', 'max:10'], 'bs_equity_fp2' => ['required', 'number', 'max:10'], 'bs_equity_fp1' => ['required', 'number', 'max:10'],
            'bs_shareholder_loans_py3' => ['required', 'number', 'max:10'], 'bs_shareholder_loans_py2' => ['required', 'number', 'max:10'],
            'bs_shareholder_loans_py1' => ['required', 'number', 'max:10'], 'bs_shareholder_loans_ytd' => ['required', 'number', 'max:10'],
            'bs_shareholder_loans_fp5' => ['required', 'number', 'max:10'], 'bs_shareholder_loans_fp4' => ['required', 'number', 'max:10'],
            'bs_shareholder_loans_fp3' => ['required', 'number', 'max:10'], 'bs_shareholder_loans_fp2' => ['required', 'number', 'max:10'],
            'bs_shareholder_loans_fp1' => ['required', 'number', 'max:10'], 'bs_capital_expenditure_py3' => ['required', 'number', 'max:10'],
            'bs_capital_expenditure_py2' => ['required', 'number', 'max:10'], 'bs_capital_expenditure_py1' => ['required', 'number', 'max:10'],
            'bs_capital_expenditure_ytd' => ['required', 'number', 'max:10'], 'bs_capital_expenditure_fp5' => ['required', 'number', 'max:10'],
            'bs_capital_expenditure_fp4' => ['required', 'number', 'max:10'], 'bs_capital_expenditure_fp3' => ['required', 'number', 'max:10'],
            'bs_capital_expenditure_fp2' => ['required', 'number', 'max:10'], 'bs_capital_expenditure_fp1' => ['required', 'number', 'max:10']
        ];
    }
}
