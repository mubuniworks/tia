<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApplicationController extends Controller
{
    /**
     *
     * Get a list of all applicants
     *
     * @param Request $request
     *
     * @return applicants
     * @return compliances
     * @return background
     * @return finacials
     * @return ratios
     **/
    public function index(Request $request)
    {
        return view('admin.applications.index')
            ->with('applicants', DB::table('applicants')->get())
            ->with('compliances', DB::table('compliances')->get())
            ->with('backgrounds', DB::table('backgrounds')->get())
            ->with('financials', DB::table('financials')->get());
    }

    /**
     *
     * show applicants registration details and calculation results (ratios)
     *
     *
     * @param int $id
     *
     * @return applicants
     * @return compliances
     * @return background
     * @return finacials
     * @return ratios
     **/
    public function show($id)
    {
        $applicant = DB::table('applicants')->where('id', $id)->get();
        $compliance = DB::table('compliances')->where('id', $id)->get();
        $background = DB::table('backgrounds')->where('id', $id)->get();
        $financial = DB::table('financials')->where('id', $id)->get();
        $ratios = DB::table('key_ratios')->where('id', $id)->get();

        return view('admin.applications.show')
            ->with('applicants', $applicant)
            ->with('compliances', $compliance)
            ->with('backgrounds', $background)
            ->with('financials', $financial)
            ->with('ratios', $ratios);
    }
}
