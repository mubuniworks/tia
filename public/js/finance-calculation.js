/*======================================================================================================
 ======================     GROSS PROFIT CALCULATIONS ============================================ */
//get revenue input values
var revenue_py3 = document.getElementById("is_revenue_py3");
var revenue_py2 = document.getElementById("is_revenue_py2");
var revenue_py1 = document.getElementById("is_revenue_py1");
var revenue_ytd = document.getElementById("is_revenue_ytd");
var revenue_fp1 = document.getElementById("is_revenue_fp1");
var revenue_fp2 = document.getElementById("is_revenue_fp2");
var revenue_fp3 = document.getElementById("is_revenue_fp3");
var revenue_fp4 = document.getElementById("is_revenue_fp4");
var revenue_fp5 = document.getElementById("is_revenue_fp5");

//get cost of sales input values
var cost_of_sales_py3 = document.getElementById("is_cost_of_sales_py3");
var cost_of_sales_py2 = document.getElementById("is_cost_of_sales_py2");
var cost_of_sales_py1 = document.getElementById("is_cost_of_sales_py1");
var cost_of_sales_ytd = document.getElementById("is_cost_of_sales_ytd");
var cost_of_sales_fp1 = document.getElementById("is_cost_of_sales_fp1");
var cost_of_sales_fp2 = document.getElementById("is_cost_of_sales_fp2");
var cost_of_sales_fp3 = document.getElementById("is_cost_of_sales_fp3");
var cost_of_sales_fp4 = document.getElementById("is_cost_of_sales_fp4");
var cost_of_sales_fp5 = document.getElementById("is_cost_of_sales_fp5");

//gross profit input values
var gross_profit_py3 = document.getElementById("is_gross_profit_py3");
var gross_profit_py2 = document.getElementById("is_gross_profit_py2");
var gross_profit_py1 = document.getElementById("is_gross_profit_py1");
var gross_profit_ytd = document.getElementById("is_gross_profit_ytd");
var gross_profit_fp1 = document.getElementById("is_gross_profit_fp1");
var gross_profit_fp2 = document.getElementById("is_gross_profit_fp2");
var gross_profit_fp3 = document.getElementById("is_gross_profit_fp3");
var gross_profit_fp4 = document.getElementById("is_gross_profit_fp4");
var gross_profit_fp5 = document.getElementById("is_gross_profit_fp5");

if (cost_of_sales_py3) { cost_of_sales_py3.addEventListener("change", function () { gross_profit_py3.value = (parseFloat(revenue_py3.value) - parseFloat(cost_of_sales_py3.value)) }); }
if (cost_of_sales_py2) { cost_of_sales_py2.addEventListener("change", function () { gross_profit_py2.value = (parseFloat(revenue_py2.value) - parseFloat(cost_of_sales_py2.value)) }); }
if (cost_of_sales_py1) { cost_of_sales_py1.addEventListener("change", function () { gross_profit_py1.value = (parseFloat(revenue_py1.value) - parseFloat(cost_of_sales_py1.value)) }); }
if (cost_of_sales_ytd) { cost_of_sales_ytd.addEventListener("change", function () { gross_profit_ytd.value = (parseFloat(revenue_ytd.value) - parseFloat(cost_of_sales_ytd.value)) }); }
if (cost_of_sales_fp1) { cost_of_sales_fp1.addEventListener("change", function () { gross_profit_fp1.value = (parseFloat(revenue_fp1.value) - parseFloat(cost_of_sales_fp1.value)) }); }
if (cost_of_sales_fp2) { cost_of_sales_fp2.addEventListener("change", function () { gross_profit_fp2.value = (parseFloat(revenue_fp2.value) - parseFloat(cost_of_sales_fp2.value)) }); }
if (cost_of_sales_fp3) { cost_of_sales_fp3.addEventListener("change", function () { gross_profit_fp3.value = (parseFloat(revenue_fp3.value) - parseFloat(cost_of_sales_fp3.value)) }); }
if (cost_of_sales_fp4) { cost_of_sales_fp4.addEventListener("change", function () { gross_profit_fp4.value = (parseFloat(revenue_fp4.value) - parseFloat(cost_of_sales_fp4.value)) }); }
if (cost_of_sales_fp5) { cost_of_sales_fp5.addEventListener("change", function () { gross_profit_fp5.value = (parseFloat(revenue_fp5.value) - parseFloat(cost_of_sales_fp5.value)) }); }



/*======================================================================================================
 ======================     PROFIT BEFORE TAX CALCULATIONS ============================================ */

//operating expenses input values
var operating_expenses_py3 = document.getElementById("is_operating_expenses_py3");
var operating_expenses_py2 = document.getElementById("is_operating_expenses_py2");
var operating_expenses_py1 = document.getElementById("is_operating_expenses_py1");
var operating_expenses_ytd = document.getElementById("is_operating_expenses_ytd");
var operating_expenses_fp1 = document.getElementById("is_operating_expenses_fp1");
var operating_expenses_fp2 = document.getElementById("is_operating_expenses_fp2");
var operating_expenses_fp3 = document.getElementById("is_operating_expenses_fp3");
var operating_expenses_fp4 = document.getElementById("is_operating_expenses_fp4");
var operating_expenses_fp5 = document.getElementById("is_operating_expenses_fp5");


//profit before tax input values
var profit_before_tax_py3 = document.getElementById("is_profit_before_tax_py3");
var profit_before_tax_py2 = document.getElementById("is_profit_before_tax_py2");
var profit_before_tax_py1 = document.getElementById("is_profit_before_tax_py1");
var profit_before_tax_ytd = document.getElementById("is_profit_before_tax_ytd");
var profit_before_tax_fp1 = document.getElementById("is_profit_before_tax_fp1");
var profit_before_tax_fp2 = document.getElementById("is_profit_before_tax_fp2");
var profit_before_tax_fp3 = document.getElementById("is_profit_before_tax_fp3");
var profit_before_tax_fp4 = document.getElementById("is_profit_before_tax_fp4");
var profit_before_tax_fp5 = document.getElementById("is_profit_before_tax_fp5");

if (operating_expenses_py3) { operating_expenses_py3.addEventListener("change", function () { profit_before_tax_py3.value = (parseFloat(gross_profit_py3.value) - parseFloat(operating_expenses_py3.value)) }); }
if (operating_expenses_py2) { operating_expenses_py2.addEventListener("change", function () { profit_before_tax_py2.value = (parseFloat(gross_profit_py2.value) - parseFloat(operating_expenses_py2.value)) }); }
if (operating_expenses_py1) { operating_expenses_py1.addEventListener("change", function () { profit_before_tax_py1.value = (parseFloat(gross_profit_py1.value) - parseFloat(operating_expenses_py1.value)) }); }
if (operating_expenses_ytd) { operating_expenses_ytd.addEventListener("change", function () { profit_before_tax_ytd.value = (parseFloat(gross_profit_ytd.value) - parseFloat(operating_expenses_ytd.value)) }); }
if (operating_expenses_fp1) { operating_expenses_fp1.addEventListener("change", function () { profit_before_tax_fp1.value = (parseFloat(gross_profit_fp1.value) - parseFloat(operating_expenses_fp1.value)) }); }
if (operating_expenses_fp2) { operating_expenses_fp2.addEventListener("change", function () { profit_before_tax_fp2.value = (parseFloat(gross_profit_fp2.value) - parseFloat(operating_expenses_fp2.value)) }); }
if (operating_expenses_fp3) { operating_expenses_fp3.addEventListener("change", function () { profit_before_tax_fp3.value = (parseFloat(gross_profit_fp3.value) - parseFloat(operating_expenses_fp3.value)) }); }
if (operating_expenses_fp4) { operating_expenses_fp4.addEventListener("change", function () { profit_before_tax_fp4.value = (parseFloat(gross_profit_fp4.value) - parseFloat(operating_expenses_fp4.value)) }); }
if (operating_expenses_fp5) { operating_expenses_fp5.addEventListener("change", function () { profit_before_tax_fp5.value = (parseFloat(gross_profit_fp5.value) - parseFloat(operating_expenses_fp5.value)) }); }



/*======================================================================================================
 ======================     PROFIT AFTER TAX CALCULATIONS ============================================ */
//profit before tax input values
var profit_after_tax_py3 = document.getElementById("is_profit_after_tax_py3");
var profit_after_tax_py2 = document.getElementById("is_profit_after_tax_py2");
var profit_after_tax_py1 = document.getElementById("is_profit_after_tax_py1");
var profit_after_tax_ytd = document.getElementById("is_profit_after_tax_ytd");
var profit_after_tax_fp1 = document.getElementById("is_profit_after_tax_fp1");
var profit_after_tax_fp2 = document.getElementById("is_profit_after_tax_fp2");
var profit_after_tax_fp3 = document.getElementById("is_profit_after_tax_fp3");
var profit_after_tax_fp4 = document.getElementById("is_profit_after_tax_fp4");
var profit_after_tax_fp5 = document.getElementById("is_profit_after_tax_fp5");


var tax_py3 = document.getElementById("is_tax_py3");
var tax_py2 = document.getElementById("is_tax_py2");
var tax_py1 = document.getElementById("is_tax_py1");
var tax_ytd = document.getElementById("is_tax_ytd");
var tax_fp1 = document.getElementById("is_tax_fp1");
var tax_fp2 = document.getElementById("is_tax_fp2");
var tax_fp3 = document.getElementById("is_tax_fp3");
var tax_fp4 = document.getElementById("is_tax_fp4");
var tax_fp5 = document.getElementById("is_tax_fp5");


if (tax_py3) { tax_py3.addEventListener("change", function () { profit_after_tax_py3.value = (parseFloat(profit_before_tax_py3.value) - parseFloat(tax_py3.value)) }); }
if (tax_py2) { tax_py2.addEventListener("change", function () { profit_after_tax_py2.value = (parseFloat(profit_before_tax_py2.value) - parseFloat(tax_py2.value)) }); }
if (tax_py1) { tax_py1.addEventListener("change", function () { profit_after_tax_py1.value = (parseFloat(profit_before_tax_py1.value) - parseFloat(tax_py1.value)) }); }
if (tax_ytd) { tax_ytd.addEventListener("change", function () { profit_after_tax_ytd.value = (parseFloat(profit_before_tax_ytd.value) - parseFloat(tax_ytd.value)) }); }
if (tax_fp1) { tax_fp1.addEventListener("change", function () { profit_after_tax_fp1.value = (parseFloat(profit_before_tax_fp1.value) - parseFloat(tax_fp1.value)) }); }
if (tax_fp2) { tax_fp2.addEventListener("change", function () { profit_after_tax_fp2.value = (parseFloat(profit_before_tax_fp2.value) - parseFloat(tax_fp2.value)) }); }
if (tax_fp3) { tax_fp3.addEventListener("change", function () { profit_after_tax_fp3.value = (parseFloat(profit_before_tax_fp3.value) - parseFloat(tax_fp3.value)) }); }
if (tax_fp4) { tax_fp4.addEventListener("change", function () { profit_after_tax_fp4.value = (parseFloat(profit_before_tax_fp4.value) - parseFloat(tax_fp4.value)) }); }
if (tax_fp5) { tax_fp5.addEventListener("change", function () { profit_after_tax_fp5.value = (parseFloat(profit_before_tax_fp5.value) - parseFloat(tax_fp5.value)) }); }


/*======================================================================================================
 ===========================  TOTAL ASSETS CALCULATIONS ============================================ */
//fixed assets + current assets
//total assets values
var total_assets_py3 = document.getElementById("bs_total_assets_py3");
var total_assets_py2 = document.getElementById("bs_total_assets_py2");
var total_assets_py1 = document.getElementById("bs_total_assets_py1");
var total_assets_ytd = document.getElementById("bs_total_assets_ytd");
var total_assets_fp1 = document.getElementById("bs_total_assets_fp1");
var total_assets_fp2 = document.getElementById("bs_total_assets_fp2");
var total_assets_fp3 = document.getElementById("bs_total_assets_fp3");
var total_assets_fp4 = document.getElementById("bs_total_assets_fp4");
var total_assets_fp5 = document.getElementById("bs_total_assets_fp5");

//fixed assets values
var fixed_assets_py3 = document.getElementById("bs_fixed_assets_py3");
var fixed_assets_py2 = document.getElementById("bs_fixed_assets_py2");
var fixed_assets_py1 = document.getElementById("bs_fixed_assets_py1");
var fixed_assets_ytd = document.getElementById("bs_fixed_assets_ytd");
var fixed_assets_fp1 = document.getElementById("bs_fixed_assets_fp1");
var fixed_assets_fp2 = document.getElementById("bs_fixed_assets_fp2");
var fixed_assets_fp3 = document.getElementById("bs_fixed_assets_fp3");
var fixed_assets_fp4 = document.getElementById("bs_fixed_assets_fp4");
var fixed_assets_fp5 = document.getElementById("bs_fixed_assets_fp5");


//total assets values
var current_assets_py3 = document.getElementById("bs_current_assets_py3");
var current_assets_py2 = document.getElementById("bs_current_assets_py2");
var current_assets_py1 = document.getElementById("bs_current_assets_py1");
var current_assets_ytd = document.getElementById("bs_current_assets_ytd");
var current_assets_fp1 = document.getElementById("bs_current_assets_fp1");
var current_assets_fp2 = document.getElementById("bs_current_assets_fp2");
var current_assets_fp3 = document.getElementById("bs_current_assets_fp3");
var current_assets_fp4 = document.getElementById("bs_current_assets_fp4");
var current_assets_fp5 = document.getElementById("bs_current_assets_fp5");


if (current_assets_py3) { current_assets_py3.addEventListener("change", function () { total_assets_py3.value = (parseFloat(fixed_assets_py3.value) + parseFloat(current_assets_py3.value)) }); }
if (current_assets_py2) { current_assets_py2.addEventListener("change", function () { total_assets_py2.value = (parseFloat(fixed_assets_py2.value) + parseFloat(current_assets_py2.value)) }); }
if (current_assets_py1) { current_assets_py1.addEventListener("change", function () { total_assets_py1.value = (parseFloat(fixed_assets_py1.value) + parseFloat(current_assets_py1.value)) }); }
if (current_assets_ytd) { current_assets_ytd.addEventListener("change", function () { total_assets_ytd.value = (parseFloat(fixed_assets_ytd.value) + parseFloat(current_assets_ytd.value)) }); }
if (current_assets_fp1) { current_assets_fp1.addEventListener("change", function () { total_assets_fp1.value = (parseFloat(fixed_assets_fp1.value) + parseFloat(current_assets_fp1.value)) }); }
if (current_assets_fp2) { current_assets_fp2.addEventListener("change", function () { total_assets_fp2.value = (parseFloat(fixed_assets_fp2.value) + parseFloat(current_assets_fp2.value)) }); }
if (current_assets_fp3) { current_assets_fp3.addEventListener("change", function () { total_assets_fp3.value = (parseFloat(fixed_assets_fp3.value) + parseFloat(current_assets_fp3.value)) }); }
if (current_assets_fp4) { current_assets_fp4.addEventListener("change", function () { total_assets_fp4.value = (parseFloat(fixed_assets_fp4.value) + parseFloat(current_assets_fp4.value)) }); }
if (current_assets_fp5) { current_assets_fp5.addEventListener("change", function () { total_assets_fp5.value = (parseFloat(fixed_assets_fp5.value) + parseFloat(current_assets_fp5.value)) }); }

/*======================================================================================================
 =========================== TOTAL LIABILITIES CALCULATIONS ============================================ */

//long term liabilities + current liabilities
//total liabilities values
var total_liabilities_py3 = document.getElementById("bs_total_liabilities_py3");
var total_liabilities_py2 = document.getElementById("bs_total_liabilities_py2");
var total_liabilities_py1 = document.getElementById("bs_total_liabilities_py1");
var total_liabilities_ytd = document.getElementById("bs_total_liabilities_ytd");
var total_liabilities_fp1 = document.getElementById("bs_total_liabilities_fp1");
var total_liabilities_fp2 = document.getElementById("bs_total_liabilities_fp2");
var total_liabilities_fp3 = document.getElementById("bs_total_liabilities_fp3");
var total_liabilities_fp4 = document.getElementById("bs_total_liabilities_fp4");
var total_liabilities_fp5 = document.getElementById("bs_total_liabilities_fp5");

//longterm liabilities values
var longterm_liabilities_py3 = document.getElementById("bs_longterm_liabilities_py3");
var longterm_liabilities_py2 = document.getElementById("bs_longterm_liabilities_py2");
var longterm_liabilities_py1 = document.getElementById("bs_longterm_liabilities_py1");
var longterm_liabilities_ytd = document.getElementById("bs_longterm_liabilities_ytd");
var longterm_liabilities_fp1 = document.getElementById("bs_longterm_liabilities_fp1");
var longterm_liabilities_fp2 = document.getElementById("bs_longterm_liabilities_fp2");
var longterm_liabilities_fp3 = document.getElementById("bs_longterm_liabilities_fp3");
var longterm_liabilities_fp4 = document.getElementById("bs_longterm_liabilities_fp4");
var longterm_liabilities_fp5 = document.getElementById("bs_longterm_liabilities_fp5");


//current liabilities values
var current_liabilities_py3 = document.getElementById("bs_current_liabilities_py3");
var current_liabilities_py2 = document.getElementById("bs_current_liabilities_py2");
var current_liabilities_py1 = document.getElementById("bs_current_liabilities_py1");
var current_liabilities_ytd = document.getElementById("bs_current_liabilities_ytd");
var current_liabilities_fp1 = document.getElementById("bs_current_liabilities_fp1");
var current_liabilities_fp2 = document.getElementById("bs_current_liabilities_fp2");
var current_liabilities_fp3 = document.getElementById("bs_current_liabilities_fp3");
var current_liabilities_fp4 = document.getElementById("bs_current_liabilities_fp4");
var current_liabilities_fp5 = document.getElementById("bs_current_liabilities_fp5");

if (current_liabilities_py3) { current_liabilities_py3.addEventListener("change", function () { total_liabilities_py3.value = (parseFloat(longterm_liabilities_py3.value) + parseFloat(current_liabilities_py3.value)) }); }
if (current_liabilities_py2) { current_liabilities_py2.addEventListener("change", function () { total_liabilities_py2.value = (parseFloat(longterm_liabilities_py2.value) + parseFloat(current_liabilities_py2.value)) }); }
if (current_liabilities_py1) { current_liabilities_py1.addEventListener("change", function () { total_liabilities_py1.value = (parseFloat(longterm_liabilities_py1.value) + parseFloat(current_liabilities_py1.value)) }); }
if (current_liabilities_ytd) { current_liabilities_ytd.addEventListener("change", function () { total_liabilities_ytd.value = (parseFloat(longterm_liabilities_ytd.value) + parseFloat(current_liabilities_ytd.value)) }); }
if (current_liabilities_fp1) { current_liabilities_fp1.addEventListener("change", function () { total_liabilities_fp1.value = (parseFloat(longterm_liabilities_fp1.value) + parseFloat(current_liabilities_fp1.value)) }); }
if (current_liabilities_fp2) { current_liabilities_fp2.addEventListener("change", function () { total_liabilities_fp2.value = (parseFloat(longterm_liabilities_fp2.value) + parseFloat(current_liabilities_fp2.value)) }); }
if (current_liabilities_fp3) { current_liabilities_fp3.addEventListener("change", function () { total_liabilities_fp3.value = (parseFloat(longterm_liabilities_fp3.value) + parseFloat(current_liabilities_fp3.value)) }); }
if (current_liabilities_fp4) { current_liabilities_fp4.addEventListener("change", function () { total_liabilities_fp4.value = (parseFloat(longterm_liabilities_fp4.value) + parseFloat(current_liabilities_fp4.value)) }); }
if (current_liabilities_fp5) { current_liabilities_fp5.addEventListener("change", function () { total_liabilities_fp5.value = (parseFloat(longterm_liabilities_fp5.value) + parseFloat(current_liabilities_fp5.value)) }); }




/*======================================================================================================
 =========================  EQUITY CALCULATIONS ============================================ */
//total assets + total liabilities


//total assets values
var equity_py3 = document.getElementById("bs_equity_py3");
var equity_py2 = document.getElementById("bs_equity_py2");
var equity_py1 = document.getElementById("bs_equity_py1");
var equity_ytd = document.getElementById("bs_equity_ytd");
var equity_fp1 = document.getElementById("bs_equity_fp1");
var equity_fp2 = document.getElementById("bs_equity_fp2");
var equity_fp3 = document.getElementById("bs_equity_fp3");
var equity_fp4 = document.getElementById("bs_equity_fp4");
var equity_fp5 = document.getElementById("bs_equity_fp5");

if (current_liabilities_py3) { current_liabilities_py3.addEventListener("change", function () { equity_py3.value = (parseFloat(total_assets_py3.value) - parseFloat(total_liabilities_py3.value)) }); }
if (current_liabilities_py2) { current_liabilities_py2.addEventListener("change", function () { equity_py2.value = (parseFloat(total_assets_py2.value) - parseFloat(total_liabilities_py2.value)) }); }
if (current_liabilities_py1) { current_liabilities_py1.addEventListener("change", function () { equity_py1.value = (parseFloat(total_assets_py1.value) - parseFloat(total_liabilities_py1.value)) }); }
if (current_liabilities_ytd) { current_liabilities_ytd.addEventListener("change", function () { equity_ytd.value = (parseFloat(total_assets_ytd.value) - parseFloat(total_liabilities_ytd.value)) }); }
if (current_liabilities_fp1) { current_liabilities_fp1.addEventListener("change", function () { equity_fp1.value = (parseFloat(total_assets_fp1.value) - parseFloat(total_liabilities_fp1.value)) }); }
if (current_liabilities_fp2) { current_liabilities_fp2.addEventListener("change", function () { equity_fp2.value = (parseFloat(total_assets_fp2.value) - parseFloat(total_liabilities_fp2.value)) }); }
if (current_liabilities_fp3) { current_liabilities_fp3.addEventListener("change", function () { equity_fp3.value = (parseFloat(total_assets_fp3.value) - parseFloat(total_liabilities_fp3.value)) }); }
if (current_liabilities_fp4) { current_liabilities_fp4.addEventListener("change", function () { equity_fp4.value = (parseFloat(total_assets_fp4.value) - parseFloat(total_liabilities_fp4.value)) }); }
if (current_liabilities_fp5) { current_liabilities_fp5.addEventListener("change", function () { equity_fp5.value = (parseFloat(total_assets_fp5.value) - parseFloat(total_liabilities_fp5.value)) }); }
