<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class BackgroundTest extends TestCase
{
    protected $background;

    /**
     * @test
     **/
    public function company_background_is_an_instance_of_a_background()
    {
        $this->assertInstanceOf('Background', $this->background);
    }
}
