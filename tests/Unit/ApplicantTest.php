<?php

namespace Tests\Unit;

use App\Applicant;
use PHPUnit\Framework\TestCase;

class ApplicantTest extends TestCase
{
    protected $applicant;

    /**
     * @test
     **/
    public function an_applicant_is_an_instance_of_an_applicant()
    {
        $this->assertInstanceOf('Applicant', $this->applicant);
    }

    /** @test */
    public function applicants_are_filtered_by_id()
    {
        $applicant = factory(Applicant::class)->create();
        $this->assertEquals(Applicant::byId($applicant->id)->count(), $applicant->count());
    }
}
