<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    protected $user;

    /**
     * @test
     **/
    public function a_user_is_an_instance_of_a_user()
    {
        $this->assertInstanceOf('App\User', $this->user);
    }

    /**
     * @test
     **/
    public function a_user_has_a_role()
    {
        $user = factory(User::class)->create();

        $this->assertTrue($user->hasRole(0));
        $this->assertTrue($user->hasRole(1));
    }
}
