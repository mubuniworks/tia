<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ApplicantDetailsExportTest extends TestCase
{
    use RefreshDatabase;


    /**
     * @test
     *
     * @return void
     */
    public function admin_can_download_applicants_list()
    {
        $users = factory(User::class)->create();

        $response = $this->get(url('/export-applicant', [$users->id, '1/0']));
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function non_admin_users_can_not_see_export_applicants_action()
    {
        $users = factory(User::class)->role(1)->create();

        $response = $this->get(url('/export-applicant', [$users->id, '1/0']));
        $response->assertDontSee('export to excel');
    }
}
