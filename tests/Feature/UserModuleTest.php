<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserModuleTest extends TestCase
{
    /**
     * @test
     *
     **/
    public function admin_can_view_all_users()
    {
        $users = factory(User::class)->create();
        $response = $this->get('all-users/' . $users->id);
        $response->assertStatus(200);
        $response->assertViewIs('admin.users.index');
        $response->assertViewHas('users');
    }


    /**
     * @test
     *
     **/
    public function admin_can_view_create_users_form()
    {
        $users = factory(User::class)->create();
        $response = $this->get('all-users/' . $users->id);
        $response->assertStatus(200);
        $response->assertViewIs('admin.users.create');
        $response->assertViewHas('name', 'lastname', 'email', 'role', 'password');
    }


    /**
     * @test
     *
     **/
    public function admin_can_add_user()
    {
        $users = factory(User::class)->raw();
        $this->followingRedirects()->post(route('admin.users.create'), $users)
            ->assertStatus(200);
    }

    /**
     * @test
     *
     **/
    public function admin_can_view_edit_users_form()
    {
        $users = factory(User::class)->create();
        $response = $this->get('edit-user/' . $users->id);
        $response->assertStatus(200);
        $response->assertViewIs('admin.users.edit');
        $response->assertViewHas('name', 'lastname', 'email', 'role', 'password');
    }


    /**
     * @test
     *
     **/
    public function admin_can_edit_users()
    {
        $users = factory(AccountSector::class)->create();
        $this->followingRedirects()
            ->patch(route('admin.users.edit', $users->id))
            ->assertStatus(200);
        $this->assertDatabaseHas('users', $users->id);
    }


    /**
     * @test
     */
    public function non_admin_users_can_not_see_add_users_forms()
    {
        $user = factory(User::class)->role(1)->create();
        $this->actingAs($user);

        $response = $this->get(url('/new-user'));
        $response->assertDontSee('add user');
    }
}
