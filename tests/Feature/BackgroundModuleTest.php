<?php

namespace Tests\Feature;

use App\Background;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BackgroundModuleTest extends TestCase
{
    /**
     * @test
     *
     **/
    public function applicant_can_view_add_background_form()
    {
        $response = $this->get(view('form.background'));
        $response->assertStatus(200);
        $response->assertViewIs('forms.background');
        $response->assertViewHas(
            'business_location_province',
            'business_nearest_city_location',
            'project_actual_location_area',
            'business_description',
            'business_financial_year_end',
            'business_accountant_name'
        );
    }

    /**
     * @test
     *
     **/
    public function applicant_can_submit_details_and_view_compliance_form()
    {
        $background = factory(Background::class)->raw();
        $this->followingRedirects()->post(route('forms.background'), $background)->assertStatus(200);

        $this->get(route('forms.compliance'))
            ->assertRedirect(route('forms.background'));
    }
}
