<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Auth::routes([
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
]);


Route::redirect('home', 'admin-dash', 301);

Route::get('/', 'RefinerController@get_applicant');
Route::post('applicant-register', 'RefinerController@applicant_registration');
Route::post('compliance-register', 'RefinerController@compliance_registration');
Route::post('background-register', 'RefinerController@background_registration');
Route::post('financial-register', 'RefinerController@financial_registration');


Route::group(['middleware' => ['auth']], function () {
    Route::get('/admin-dash', 'AdminController@index');

    Route::get('all-users', 'UserController@index');
    Route::get('new-user', 'UserController@create');
    Route::post('new-user', 'UserController@store');
    Route::get('show-user/{id}', 'UserController@show');
    Route::get('/edit-user/{id}', 'UserController@edit');
    Route::post('/update-user/{id}', 'UserController@update');

    Route::get('applications', 'ApplicationController@index');
    Route::get('show-application/{id}', 'ApplicationController@show');

    Route::get('tier1', 'RefinerController@tier_one');
    Route::get('tier2', 'RefinerController@tier_two');
    Route::get('refiner', 'RefinerController@revenue_growth');

    //applicant data exports
    Route::get('/export-applicant/{id}', 'ExportController@export');
});
