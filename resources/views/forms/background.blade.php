@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">

                <div class="card mt-5">
                    <div class="card-header bg-primary text-white font-weight-bold text-uppercase rounded-0 pt-3 pb-3">{{ __('COMPANY BACKGROUND FORM') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ url('background-register') }}" class="pr-5 pl-5" autocomplete="off">
                            @csrf
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label class="col-form-label font-weight-bold">
                                        Shareholding information
                                    </label><br>
                                    <span class="m-form__help font-weight-bold">
                                        <small>(if individual)</small>
                                    </span><br>
                                    <span class="m-form__help font-weight-bold">
                                        Name
                                    </span>

                                    @if (Session::has('session_backgrounds'))
                                        @foreach ($session_backgrounds as $background)
                                            <div class="input-group">
                                                <input type="text" name="shareholder_name" id="shareholder_name" class="form-control m-input rounded-0 font-weight-bold pt-3 pb-3" value="{{ $background['shareholder_name'] }}" autofocus>
                                            </div>
                                        @endforeach
                                    @else
                                        <div class="input-group">
                                            <input type="text" name="shareholder_name" id="shareholder_name" class="form-control m-input rounded-0 font-weight-bold pt-3 pb-3" value="{{ old('shareholder_name') }}" autofocus>
                                        </div>
                                    @endif
                                </div>

                                <div class="col-md-6">
                                    <label class="col-form-label mb-3"></label><br>
                                    <span class="m-form__help font-weight-bold"></span><br>
                                    <span class="m-form__help font-weight-bold mb-3">
                                        ID
                                    </span>

                                    @if (Session::has('session_backgrounds'))
                                        @foreach ($session_backgrounds as $background)
                                            <div class="input-group">
                                                <input type="number" name="shareholder_id" id="shareholder_id" class="form-control m-input rounded-0 font-weight-bold pt-3 pb-3" value="{{ $background['shareholder_id'] }}">
                                            </div>
                                        @endforeach
                                    @else
                                        <div class="input-group">
                                            <input type="number" name="shareholder_id" id="shareholder_id" class="form-control m-input rounded-0 font-weight-bold pt-3 pb-3" value="{{ old('shareholder_id') }}">
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label class="col-form-label"></label>
                                    <span class="m-form__help font-weight-bold">
                                        <small>(if legal entity)</small>
                                    </span><br>
                                    <span class="m-form__help font-weight-bold">
                                        Entity name
                                    </span>

                                    @if (Session::has('session_backgrounds'))
                                        @foreach ($session_backgrounds as $background)
                                            <div class="input-group">
                                                <input type="text" name="entity_name" id="entity_name" class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3" value="{{ $background['entity_name'] }}">
                                            </div>
                                        @endforeach
                                    @else
                                        <div class="input-group">
                                            <input type="text" name="entity_name" id="entity_name" class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3" value="{{ old('entity_name') }}">
                                        </div>
                                    @endif
                                </div>

                                <div class="col-md-6">
                                    <label class="col-form-label"></label>
                                    <div>
                                        <span class="m-form__help font-weight-bold">
                                            company/trust registration number
                                        </span>

                                        @if (Session::has('session_backgrounds'))
                                            @foreach ($session_backgrounds as $background)
                                                <div class="input-group">
                                                    <input type="text" name="entity_registration_number" id="entity_registration_number" class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3" value="{{ $background['entity_registration_number'] }}">
                                                </div>
                                            @endforeach
                                        @else
                                            <div class="input-group">
                                                <input type="text" name="entity_registration_number" id="entity_registration_number" class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3" value="{{ old('entity_registration_number') }}">
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div><hr>

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label class="col-form-label font-weight-bold">
                                        Business Status
                                    </label>

                                    <div class="input-group">
                                        @if (Session::has('session_backgrounds'))
                                            @foreach ($form_applicants as $applicant)
                                                <select name="business_status" id="business_status" class="form-control rounded-0">
                                                    @foreach ($form_applicants as $app)
                                                        <option value="" hidden>Select Business Status</option>
                                                        @if ($app['business_status'] == 0)
                                                            <option value="{{ $app['business_status'] }}" {{ old('business_status') == $app['business_status'] ? "selected" :""}} selected>Start up</option>
                                                        @elseif ($app['business_status'] == 1)
                                                            <option value="{{ $app['business_status'] }}" {{ old('business_status') == $app['business_status'] ? "selected" :""}} selected>Existing Business</option>
                                                        @else

                                                        @endif
                                                    @endforeach
                                                </select>
                                            @endforeach
                                        @else
                                            <select name="business_status" id="business_status" class="form-control rounded-0">
                                                @foreach ($form_applicants as $app)
                                                    <option value="" hidden>Select Business Status</option>
                                                    @if ($app['business_status'] == 0)
                                                        <option value="{{ $app['business_status'] }}" {{ old('business_status') == $app['business_status'] ? "selected" :""}} selected>Start up</option>
                                                    @elseif ($app['business_status'] == 1)
                                                        <option value="{{ $app['business_status'] }}" {{ old('business_status') == $app['business_status'] ? "selected" :""}} selected>Existing Business</option>
                                                    @else

                                                    @endif
                                                @endforeach
                                            </select>
                                        @endif
                                    </div>
                                </div>
                                <col-xl-6 class="col-lg-6"></col-xl-6>
                            </div><hr>

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label class="col-form-label font-weight-bold">
                                        Shareholding information
                                    </label><br>
                                    <span class="m-form__help font-weight-bold">
                                        <small>(if individual)</small>
                                    </span><br>
                                    <span class="m-form__help font-weight-bold">
                                        Percentage (provide number i.e 10, 20 etc)
                                    </span>

                                    @if (Session::has('session_backgrounds'))
                                        @foreach ($session_backgrounds as $background)
                                            <div class="input-group">
                                                <input type="text" name="individual_shareholder_percentage" id="individual_shareholder_percentage" class="form-control m-input rounded-0 font-weight-bold pt-3 pb-3" value="{{ $background['individual_shareholder_percentage'] }}" >
                                            </div>
                                        @endforeach
                                    @else
                                        <div class="input-group">
                                            <input type="text" name="individual_shareholder_percentage" id="individual_shareholder_percentage" class="form-control m-input rounded-0 font-weight-bold pt-3 pb-3" value="{{ old('individual_shareholder_percentage') }}" >
                                        </div>
                                    @endif
                                </div>

                                <div class="col-md-6">
                                    <label class="col-form-label mb-3"></label><br>
                                    <span class="m-form__help font-weight-bold">
                                        <small>(if Legal Entity)</small>
                                    </span><br>
                                    <span class="m-form__help font-weight-bold">
                                        Percentage (provide number i.e 10, 20 etc)
                                    </span>

                                    @if (Session::has('session_backgrounds'))
                                        @foreach ($session_backgrounds as $background)
                                            <div class="input-group">
                                                <input type="text" name="entity_shareholder_percentage" id="entity_shareholder_percentage" class="form-control m-input rounded-0 font-weight-bold pt-3 pb-3" value="{{ $background['entity_shareholder_percentage'] }}" >
                                            </div>
                                        @endforeach
                                    @else
                                        <div class="input-group">
                                            <input type="text" name="entity_shareholder_percentage" id="entity_shareholder_percentage" class="form-control m-input rounded-0 font-weight-bold pt-3 pb-3" value="{{ old('entity_shareholder_percentage') }}" >
                                        </div>
                                    @endif
                                </div>
                            </div><hr>

                            <div class="form-group">
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label class="col-form-label font-weight-bold">
                                            Management Information <small>(Management company details)</small>
                                        </label><br>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <span class="m-form__help font-weight-bold">
                                            Name
                                        </span>

                                        @if (Session::has('session_backgrounds'))
                                            @foreach ($session_backgrounds as $background)
                                                <div class="input-group">
                                                    <input type="text" name="management_company_name" id="management_company_name" class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3" value="{{ $background['management_company_name'] }}">
                                                </div>
                                            @endforeach
                                        @else
                                            <div class="input-group">
                                                <input type="text" name="management_company_name" id="management_company_name" class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3" value="{{ old('management_company_name') }}">
                                            </div>
                                        @endif
                                    </div>

                                    <div class="col-md-4">
                                        <span class="m-form__help font-weight-bold">
                                            Identity Number
                                        </span>

                                        @if (Session::has('session_backgrounds'))
                                            @foreach ($session_backgrounds as $background)
                                                <div class="input-group">
                                                    <input type="number" name="management_registration_number" id="management_registration_number" class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3" value="{{ $background['management_registration_number'] }}">
                                                </div>
                                            @endforeach
                                        @else
                                            <div class="input-group">
                                                <input type="number" name="management_registration_number" id="management_registration_number" class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3" value="{{ old('management_registration_number') }}">
                                            </div>
                                        @endif
                                    </div>

                                    <div class="col-md-4">
                                        <span class="m-form__help font-weight-bold">
                                            Position
                                        </span>

                                        @if (Session::has('session_backgrounds'))
                                            @foreach ($session_backgrounds as $background)
                                                <div class="input-group">
                                                    <input type="text" name="management_company_position" id="management_company_position" class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3" value="{{ $background['management_company_position'] }}">
                                                </div>
                                            @endforeach
                                        @else
                                            <div class="input-group">
                                                <input type="text" name="management_company_position" id="management_company_position" class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3" value="{{ old('management_company_position') }}">
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div><hr>

                            <div class="form-group">
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label class="form-label font-weight-bold">
                                            Total Staff Compliment <small>(total staff employed by the business)</small>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label class="col-form-label font-weight-bold">
                                            Temporary
                                        </label>

                                        @if (Session::has('session_backgrounds'))
                                            @foreach ($session_backgrounds as $background)
                                                <div class="input-group">
                                                    <input type="text" name="temporary_staff_no" id="temporary_staff_no" class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3" value="{{ $background['temporary_staff_no'] }}" >
                                                </div>
                                            @endforeach
                                        @else
                                            <div class="input-group">
                                                <input type="text" name="temporary_staff_no" id="temporary_staff_no" class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3" value="{{ old('temporary_staff_no') }}" >
                                            </div>
                                        @endif
                                    </div>

                                    <div class="col-md-6">
                                        <label class="col-form-label font-weight-bold">
                                            Permanent
                                        </label>

                                        @if (Session::has('session_backgrounds'))
                                            @foreach ($session_backgrounds as $background)
                                                <div class="input-group">
                                                    <input type="text" name="permanent_staff_no" id="permanent_staff_no" class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3" value="{{ $background['permanent_staff_no'] }}" >
                                                </div>
                                            @endforeach
                                        @else
                                            <div class="input-group">
                                                <input type="text" name="permanent_staff_no" id="permanent_staff_no" class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3" value="{{ old('permanent_staff_no') }}" >
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div><hr>

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label class="col-form-label font-weight-bold">
                                        Location - province
                                    </label>

                                    <div class="input-group">
                                        @if (Session::has('session_backgrounds'))
                                            @foreach ($session_backgrounds as $background)
                                                <input type="text" name="business_location_province" id="business_location_province" class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3 @error('business_location_province') is-invalid @enderror" value="{{ $background['business_location_province'] }}" >
                                            @endforeach
                                        @else
                                            <input type="text" name="business_location_province" id="business_location_province" class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3 @error('business_location_province') is-invalid @enderror" value="{{ old('business_location_province') }}" >
                                        @endif

                                        @error('business_location_province')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <span class="m-form__help">
                                        <small class="text-muted"> province in which location the business/project operates or will operate </small>
                                    </span>
                                </div>
                                <div class="col-md-6">
                                    <label class="col-form-label font-weight-bold">
                                        Location - city
                                    </label>

                                    <div class="input-group">
                                        @if (Session::has('session_backgrounds'))
                                            @foreach ($session_backgrounds as $background)
                                                <input type="text" name="business_nearest_city_location" id="business_nearest_city_location" class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3 @error('business_nearest_city_location') is-invalid @enderror" value="{{ $background['business_nearest_city_location'] }}" >
                                            @endforeach
                                        @else
                                            <input type="text" name="business_nearest_city_location" id="business_nearest_city_location" class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3 @error('business_nearest_city_location') is-invalid @enderror" value="{{ old('business_nearest_city_location') }}" >
                                        @endif

                                        @error('business_nearest_city_location')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <span class="m-form__help">
                                        <small class="text-muted"> Nearest city in which the business/project operates </small>
                                    </span>
                                </div>
                            </div><hr>

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label class="col-form-label font-weight-bold">
                                        Project location
                                    </label>

                                    <div class="input-group">
                                        @if (Session::has('session_backgrounds'))
                                            @foreach ($session_backgrounds as $backgroundground)
                                                <input type="text" name="project_actual_location_area" id="project_actual_location_area" class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3 @error('project_actual_location_area') is-invalid @enderror" value="{{ $backgroundground['project_actual_location_area'] }}" >
                                            @endforeach
                                        @else
                                            <input type="text" name="project_actual_location_area" id="project_actual_location_area" class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3 @error('project_actual_location_area') is-invalid @enderror" value="{{ old('project_actual_location_area') }}" >
                                        @endif

                                        @error('project_actual_location_area')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <span class="m-form__help">
                                       <small class="text-muted">  Actual area location in which the business/project operates </small>
                                    </span>
                                </div>

                                <div class="col-md-6">
                                    <label class="col-form-label font-weight-bold">
                                        Business/Project desctiption
                                    </label>

                                    <div class="input-group">
                                        @if (Session::has('session_backgrounds'))
                                            @foreach ($session_backgrounds as $background)
                                                <textarea class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3 @error('business_description') is-invalid @enderror" name="business_description" id="business_description" cols="30" rows="6" value="{{ $background['business_description'] }}"></textarea>
                                            @endforeach
                                        @else
                                            <textarea class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3 @error('business_description') is-invalid @enderror" name="business_description" id="business_description" cols="30" rows="6" value="{{ old('business_description') }}"></textarea>
                                        @endif
                                    </div>

                                    <span class="m-form__help">
                                        <small class="text-muted"> overview of the business/project </small>
                                    </span>
                                    <span class="m-form__help">
                                        <small class="text-muted"> description of the products/services offered </small>
                                    </span>
                                </div>
                            </div><hr>

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label class="col-form-label font-weight-bold">
                                        Go to market strategy
                                    </label>

                                    <div class="input-group">
                                        @if (Session::has('session_backgrounds'))
                                            @foreach ($session_backgrounds as $background)
                                                <textarea class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3" name="business_goto_strategy" id="business_goto_strategy" cols="30" rows="6" value="{{ $background['business_goto_strategy'] }}"></textarea>
                                            @endforeach
                                        @else
                                            <textarea class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3" name="business_goto_strategy" id="business_goto_strategy" cols="30" rows="6" value="{{ old('business_goto_strategy') }}"></textarea>
                                        @endif
                                    </div>

                                    <span class="m-form__help">
                                        <small class="text-muted"> how is the business selling its services/products to its clientele? </small>
                                    </span>
                                    <span class="m-form__help">
                                        <small class="text-muted"> who is its target market? </small>
                                    </span>
                                    <span class="m-form__help">
                                        <small class="text-muted"> how does the business promote its services? </small>
                                    </span>
                                </div>

                                <div class="col-md-6">
                                    <label class="col-form-label font-weight-bold">
                                        Competitive advantage
                                    </label>

                                    <div class="input-group">
                                        @if (Session::has('session_backgrounds'))
                                            @foreach ($session_backgrounds as $background)
                                                <textarea class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3" name="business_competitive_advantage" id="business_competitive_advantage" cols="30" rows="6" value="{{ $background['business_competitive_advantage'] }}"></textarea>
                                            @endforeach
                                        @else
                                            <textarea class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3" name="business_competitive_advantage" id="business_competitive_advantage" cols="30" rows="6" value="{{ old('business_competitive_advantage') }}"></textarea>
                                        @endif
                                    </div>

                                    <span class="m-form__help">
                                        <small class="text-muted"> what sets apart the business from its competitors?</small>
                                    </span>
                                    <span class="m-form__help">
                                        <small class="text-muted"> what are its strengths?</small>
                                    </span>
                                </div>
                            </div><hr>

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label class="col-form-label font-weight-bold">
                                        Areas of improvement
                                    </label>

                                    <div class="input-group">
                                        @if (Session::has('session_backgrounds'))
                                            @foreach ($session_backgrounds as $background)
                                                <textarea class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3" name="business_areas_of_improvement" id="business_areas_of_improvement" cols="30" rows="6" value="{{ $background['business_areas_of_improvement'] }}"></textarea>
                                            @endforeach
                                        @else
                                            <textarea class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3" name="business_areas_of_improvement" id="business_areas_of_improvement" cols="30" rows="6"value="{{ old('business_areas_of_improvement') }}"></textarea>
                                        @endif
                                    </div>

                                    <span class="m-form__help font-weight-bold">
                                        <small class="text-muted"> which parts of the business need improvement? </small>
                                    </span>
                                    <span class="m-form__help font-weight-bold">
                                        <small class="text-muted"> what are the key issues in those areas? </small>
                                    </span>
                                    <span class="m-form__help font-weight-bold">
                                        <small class="text-muted"> what plans do you have in place to improve these areas of concern? </small>
                                    </span>
                                </div>

                                <div class="col-md-6">
                                    <label class="col-form-label font-weight-bold">
                                        Strategic opportunities
                                    </label>

                                    <div class="input-group">
                                        @if (Session::has('session_backgrounds'))
                                            @foreach ($session_backgrounds as $background)
                                                <textarea class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3" name="business_strategic_opportunities" id="business_strategic_opportunities" cols="30" rows="6" value="{{ $background['business_strategic_opportunities'] }}"></textarea>
                                            @endforeach
                                        @else
                                            <textarea class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3" name="business_strategic_opportunities" id="business_strategic_opportunities" cols="30" rows="6" value="{{ old('business_strategic_opportunities') }}"></textarea>
                                        @endif
                                    </div>

                                    <span class="m-form__help">
                                        <small class="text-muted"> what are the key commercially viable strategic oppportunities that the business can pursue?</small>
                                    </span>
                                    <span class="m-form__help">
                                        <small class="text-muted"> what is the gross estimated value of this opportunities?</small>
                                    </span>
                                    <span class="m-form__help">
                                        <small class="text-muted"> how will they transform the business and community?</small>
                                    </span>
                                </div>
                            </div><hr>

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label class="col-form-label font-weight-bold">
                                        Final Year End
                                    </label>

                                    <div class="input-group">
                                        @if (Session::has('session_backgrounds'))
                                            @foreach ($session_backgrounds as $background)
                                                <input type="text" name="business_financial_year_end" id="business_financial_year_end" class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3  @error('business_financial_year_end') is-invalid @enderror" value="{{ $background['business_financial_year_end'] }}" >
                                            @endforeach
                                        @else
                                            <input type="text" name="business_financial_year_end" id="business_financial_year_end" class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3  @error('business_financial_year_end') is-invalid @enderror" value="{{ old('business_financial_year_end') }}" >
                                        @endif

                                        @error('business_financial_year_end')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <span class="m-form__help">
                                        <small class="text-muted"> what month does the business financial year end? </small>
                                    </span>
                                </div>

                                <div class="col-md-6">
                                    <label class="col-form-label font-weight-bold">
                                        Accountant Name
                                    </label>

                                    <div class="input-group">
                                        @if (Session::has('session_backgrounds'))
                                            @foreach ($session_backgrounds as $background)
                                                <input type="text" name="business_accountant_name" id="business_accountant_name" class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3  @error('business_accountant_name') is-invalid @enderror" value="{{ $background['business_accountant_name'] }}">
                                            @endforeach
                                        @else
                                            <input type="text" name="business_accountant_name" id="business_accountant_name" class="form-control m-input rounded-0  font-weight-bold pt-3 pb-3  @error('business_accountant_name') is-invalid @enderror" value="{{ old('business_accountant_name') }}">
                                        @endif

                                        @error('business_accountant_name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <span class="m-form__help">
                                        <small class="text-muted"> details of the accounting firm that prepares your financial statements </small>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6">
                                    @foreach ($form_compliants as $comp)
                                        <input type="text" name="company_name" id="" value="{{ $comp['company_name'] }}" hidden>
                                        <input type="text" name="registration_no" id="" value="{{ $comp['registration_no'] }}" hidden>
                                        <input type="text" name="tax_registration_no" id="" value="{{ $comp['tax_registration_no'] }}" hidden>
                                        <input type="text" name="vat_registration_no" id="" value="{{ $comp['vat_registration_no'] }}" hidden>
                                        <input type="text" name="contact_person" id="" value="{{ $comp['contact_person'] }}" hidden>
                                        <input type="text" name="contact_person_email" id="" value="{{ $comp['contact_person_email'] }}" hidden>
                                        <input type="text" name="contact_person_mobile" id="" value="{{ $comp['contact_person_mobile'] }}" hidden>
                                    @endforeach

                                    @foreach ($form_applicants as $app)
                                        <input type="text" name="firstname" id="" value="{{ $app['firstname'] }}" hidden>
                                        <input type="text" name="lastname" id="" value="{{ $app['lastname'] }}" hidden>
                                        <input type="text" name="race" id="" value="{{ $app['race'] }}" hidden>
                                        <input type="text" name="nationality" id="" value="{{ $app['nationality'] }}" hidden>
                                        <input type="text" name="email" id="" value="{{ $app['email'] }}" hidden>
                                        <input type="text" name="phone" id="" value="{{ $app['phone'] }}" hidden>
                                        <input type="text" name="business_status" id="" value="{{ $app['business_status'] }}" hidden>
                                    @endforeach
                                </div>
                                <div class="col-md-6"></div>
                            </div>

                            <div class="form-group row pt-3">
                                <div class="col-md-8">
                                    <button type="submit" name="submit" class="btn btn-primary font-weight-bold pr-5 pl-5 mr-3">
                                        {{ __('Submit') }}
                                    </button>

                                    <button type="submit" name="save" class="btn btn-outline-primary font-weight-bold pr-5 pl-5">
                                        {{ __('Save for Later') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
