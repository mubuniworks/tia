@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                @if (session('success_message'))
                    <div class="alert alert-success font-weight-bold">{{ session('success_message') }}</div>
                @elseif (session('error_message'))
                    <div class="alert alert-danger font-weight-bold">{{ session('error_message') }}</div>
                @elseif (session('warning_message'))
                    <div class="alert alert-warning font-weight-bold">{{ session('warning_message') }}</div>
                @elseif (session('info_message'))
                    <div class="alert alert-info font-weight-bold">{{ session('info_message') }}</div>
                @else
                @endif

                <div class="card mt-5">
                    <div class="card-header bg-primary text-white font-weight-bold text-uppercase rounded-0 pt-3 pb-3">{{ __('FINANCIAL INFORMATION') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ url('financial-register') }}" class="pr-5 pl-5" autocomplete="off">
                            @csrf
                            <div class="row">
                                <div class="m-form__section m-form__section--first">
                                    <div class="m-form__heading">
                                        <h5 class="m-form__heading-title">
                                            Income Stetement Overview <strong>(PY - Prior Year | FP - Forecast Period | YTD - Year To Date)</strong><br>
                                        </h5>
                                        <h6 class="font-weight-bold font-italic">Note : Incase of no financial information input 0 as the value. </h6>
                                    </div>
                                </div>

                                <div class="m-form__section">
                                    <div class="form-group m-form__group">
                                        <label class="col-xl-2 col-lg-2 col-form-label">
                                            <strong>Revenue</strong>
                                        </label>
                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help"> PY -3 </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_revenue_py3" name="is_revenue_py3" class="@error ('is_revenue_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_revenue_py3'] }}" autofocus>
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_revenue_py3" name="is_revenue_py3" class="@error ('is_revenue_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_revenue_py3') }}" autofocus>
                                                @endif

                                                @error("is_revenue_py3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_revenue_py2" name="is_revenue_py2" class="@error ('is_revenue_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_revenue_py2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_revenue_py2" name="is_revenue_py2" class="@error ('is_revenue_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_revenue_py2') }}" >
                                                @endif

                                                @error("is_revenue_py2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_revenue_py1" name="is_revenue_py1" class="@error ('is_revenue_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_revenue_py1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_revenue_py1" name="is_revenue_py1" class="@error ('is_revenue_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_revenue_py1') }}" >
                                                @endif


                                                @error("is_revenue_py1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3">
                                                <span class="m-form__help">
                                                    YTD
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_revenue_ytd" name="is_revenue_ytd" class="@error ('is_revenue_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_revenue_ytd'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_revenue_ytd" name="is_revenue_ytd" class="@error ('is_revenue_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_revenue_ytd') }}" >
                                                @endif


                                                @error("is_revenue_ytd")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_revenue_fp1" name="is_revenue_fp1" class="@error ('is_revenue_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_revenue_fp1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_revenue_fp1" name="is_revenue_fp1" class="@error ('is_revenue_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_revenue_fp1') }}" >
                                                @endif


                                                @error("is_revenue_fp1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_revenue_fp2" name="is_revenue_fp2" class="@error ('is_revenue_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_revenue_fp2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_revenue_fp2" name="is_revenue_fp2" class="@error ('is_revenue_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{
                                                old('is_revenue_fp2') }}" >
                                                @endif

                                                @error("is_revenue_fp2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_revenue_fp3" name="is_revenue_fp3" class="@error ('is_revenue_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_revenue_fp3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_revenue_fp3" name="is_revenue_fp3" class="@error ('is_revenue_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_revenue_fp3') }}" >
                                                @endif

                                                @error("is_revenue_fp3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 4
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_revenue_fp4" name="is_revenue_fp4" class="@error ('is_revenue_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_revenue_fp4'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_revenue_fp4" name="is_revenue_fp4" class="@error ('is_revenue_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_revenue_fp4') }}" >
                                                @endif

                                                @error("is_revenue_fp4")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 5
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_revenue_fp5" name="is_revenue_fp5" class="@error ('is_revenue_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_revenue_fp5'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_revenue_fp5" name="is_revenue_fp5" class="@error ('is_revenue_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_revenue_fp5') }}" >
                                                @endif

                                                @error("is_revenue_fp5")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div><hr>

                                    <div class="form-group m-form__group">
                                        <label class="col-xl-2 col-lg-2 col-form-label">
                                            <strong> Cost of Sales: </strong>
                                        </label>
                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_cost_of_sales_py3" name="is_cost_of_sales_py3" class="@error ('is_cost_of_sales_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_cost_of_sales_py3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_cost_of_sales_py3" name="is_cost_of_sales_py3" class="@error ('is_cost_of_sales_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_cost_of_sales_py3') }}" >
                                                @endif

                                                @error("is_cost_of_sales_py3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_cost_of_sales_py2" name="is_cost_of_sales_py2" class="@error ('is_cost_of_sales_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_cost_of_sales_py2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_cost_of_sales_py2" name="is_cost_of_sales_py2" class="@error ('is_cost_of_sales_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_cost_of_sales_py2') }}" >
                                                @endif

                                                @error("is_cost_of_sales_py2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_cost_of_sales_py1" name="is_cost_of_sales_py1" class="@error ('is_cost_of_sales_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_cost_of_sales_py1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_cost_of_sales_py1" name="is_cost_of_sales_py1" class="@error ('is_cost_of_sales_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_cost_of_sales_py1') }}" >
                                                @endif

                                                @error("is_cost_of_sales_py1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3">
                                                <span class="m-form__help">
                                                    YTD
                                                </span>
                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_cost_of_sales_ytd" name="is_cost_of_sales_ytd" class="@error ('is_cost_of_sales_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_cost_of_sales_ytd'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_cost_of_sales_ytd" name="is_cost_of_sales_ytd" class="@error ('is_cost_of_sales_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_cost_of_sales_ytd') }}" >
                                                @endif

                                                @error("is_cost_of_sales_ytd")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_cost_of_sales_fp1" name="is_cost_of_sales_fp1" class="@error ('is_cost_of_sales_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_cost_of_sales_fp1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_cost_of_sales_fp1" name="is_cost_of_sales_fp1" class="@error ('is_cost_of_sales_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_cost_of_sales_fp1') }}" >
                                                @endif

                                                @error("is_cost_of_sales_fp1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_cost_of_sales_fp2" name="is_cost_of_sales_fp2" class="@error ('is_cost_of_sales_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_cost_of_sales_fp2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_cost_of_sales_fp2" name="is_cost_of_sales_fp2" class="@error ('is_cost_of_sales_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_cost_of_sales_fp2') }}" >
                                                @endif

                                                @error("is_cost_of_sales_fp2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_cost_of_sales_fp3" name="is_cost_of_sales_fp3" class="@error ('is_cost_of_sales_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_cost_of_sales_fp3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_cost_of_sales_fp3" name="is_cost_of_sales_fp3" class="@error ('is_cost_of_sales_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_cost_of_sales_fp3') }}" >
                                                @endif

                                                @error("is_cost_of_sales_fp3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 4
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_cost_of_sales_fp4" name="is_cost_of_sales_fp4" class="@error ('is_cost_of_sales_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_cost_of_sales_fp4'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_cost_of_sales_fp4" name="is_cost_of_sales_fp4" class="@error ('is_cost_of_sales_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_cost_of_sales_fp4') }}" >
                                                @endif

                                                @error("is_cost_of_sales_fp4")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 5
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_cost_of_sales_fp5" name="is_cost_of_sales_fp5" class="@error ('is_cost_of_sales_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_cost_of_sales_fp5'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_cost_of_sales_fp5" name="is_cost_of_sales_fp5" class="@error ('is_cost_of_sales_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_cost_of_sales_fp5') }}" >
                                                @endif

                                                @error("is_cost_of_sales_fp5")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div><hr>

                                    <div class="form-group m-form__group">
                                        <label class="col-xl-2 col-lg-2 col-form-label">
                                            <strong> Gross Profit: </strong>
                                        </label>
                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_gross_profit_py3" name="is_gross_profit_py3" class="@error ('is_gross_profit_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_gross_profit_py3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_gross_profit_py3" name="is_gross_profit_py3" class="@error ('is_gross_profit_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_gross_profit_py3') }}" >
                                                @endif

                                                @error("is_gross_profit_py3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_gross_profit_py2" name="is_gross_profit_py2" class="@error ('is_gross_profit_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_gross_profit_py2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_gross_profit_py2" name="is_gross_profit_py2" class="@error ('is_gross_profit_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_gross_profit_py2') }}" >
                                                @endif

                                                @error("is_gross_profit_py2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_gross_profit_py1" name="is_gross_profit_py1" class="@error ('is_gross_profit_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_gross_profit_py1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_gross_profit_py1" name="is_gross_profit_py1" class="@error ('is_gross_profit_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_gross_profit_py1') }}" >
                                                @endif

                                                @error("is_gross_profit_py1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3">
                                                <span class="m-form__help">
                                                    YTD
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_gross_profit_ytd" name="is_gross_profit_ytd" class="@error ('is_gross_profit_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_gross_profit_ytd'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_gross_profit_ytd" name="is_gross_profit_ytd" class="@error ('is_gross_profit_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_gross_profit_ytd') }}" >
                                                @endif

                                                @error("is_gross_profit_ytd")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_gross_profit_fp1" name="is_gross_profit_fp1" class="@error ('is_gross_profit_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_gross_profit_fp1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_gross_profit_fp1" name="is_gross_profit_fp1" class="@error ('is_gross_profit_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_gross_profit_fp1') }}" >
                                                @endif

                                                @error("is_gross_profit_fp1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_gross_profit_fp2" name="is_gross_profit_fp2" class="@error ('is_gross_profit_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_gross_profit_fp2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_gross_profit_fp2" name="is_gross_profit_fp2" class="@error ('is_gross_profit_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_gross_profit_fp2') }}" >
                                                @endif

                                                @error("is_gross_profit_fp2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_gross_profit_fp3" name="is_gross_profit_fp3" class="@error ('is_gross_profit_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_gross_profit_fp3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_gross_profit_fp3" name="is_gross_profit_fp3" class="@error ('is_gross_profit_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_gross_profit_fp3') }}" >
                                                @endif

                                                @error("is_gross_profit_fp3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 4
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_gross_profit_fp4" name="is_gross_profit_fp4" class="@error ('is_gross_profit_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_gross_profit_fp4'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_gross_profit_fp4" name="is_gross_profit_fp4" class="@error ('is_gross_profit_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_gross_profit_fp4') }}" >
                                                @endif

                                                @error("is_gross_profit_fp4")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 5
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_gross_profit_fp5" name="is_gross_profit_fp5" class="@error ('is_gross_profit_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_gross_profit_fp5'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_gross_profit_fp5" name="is_gross_profit_fp5" class="@error ('is_gross_profit_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_gross_profit_fp5') }}" >
                                                @endif

                                                @error("is_gross_profit_fp5")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div><hr>

                                    <div class="form-group m-form__group">
                                        <label class="col-md-3 col-form-label">
                                            <strong> Operating Expenses: </strong>
                                        </label>
                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_operating_expenses_py3" name="is_operating_expenses_py3" class="@error ('is_operating_expenses_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_operating_expenses_py3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_operating_expenses_py3" name="is_operating_expenses_py3" class="@error ('is_operating_expenses_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_operating_expenses_py3') }}" >
                                                @endif


                                                @error("is_operating_expenses_py3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_operating_expenses_py2" name="is_operating_expenses_py2" class="@error ('is_operating_expenses_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_operating_expenses_py2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_operating_expenses_py2" name="is_operating_expenses_py2" class="@error ('is_operating_expenses_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_operating_expenses_py2') }}" >
                                                @endif


                                                @error("is_operating_expenses_py2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_operating_expenses_py1" name="is_operating_expenses_py1" class="@error ('is_operating_expenses_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_operating_expenses_py1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_operating_expenses_py1" name="is_operating_expenses_py1" class="@error ('is_operating_expenses_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_operating_expenses_py1') }}" >
                                                @endif


                                                @error("is_operating_expenses_py1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3">
                                                <span class="m-form__help">
                                                    YTD
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_operating_expenses_ytd" name="is_operating_expenses_ytd" class="@error ('is_operating_expenses_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_operating_expenses_ytd'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_operating_expenses_ytd" name="is_operating_expenses_ytd" class="@error ('is_operating_expenses_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_operating_expenses_ytd') }}" >
                                                @endif

                                                @error("is_operating_expenses_ytd")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_operating_expenses_fp1" name="is_operating_expenses_fp1" class="@error ('is_operating_expenses_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_operating_expenses_fp1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_operating_expenses_fp1" name="is_operating_expenses_fp1" class="@error ('is_operating_expenses_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_operating_expenses_fp1') }}" >
                                                @endif


                                                @error("is_operating_expenses_fp1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_operating_expenses_fp2" name="is_operating_expenses_fp2" class="@error ('is_operating_expenses_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_operating_expenses_fp2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_operating_expenses_fp2" name="is_operating_expenses_fp2" class="@error ('is_operating_expenses_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_operating_expenses_fp2') }}" >
                                                @endif


                                                @error("is_operating_expenses_fp2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_operating_expenses_fp3" name="is_operating_expenses_fp3" class="@error ('is_operating_expenses_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_operating_expenses_fp3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_operating_expenses_fp3" name="is_operating_expenses_fp3" class="@error ('is_operating_expenses_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_operating_expenses_fp3') }}" >
                                                @endif


                                                @error("is_operating_expenses_fp3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 4
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_operating_expenses_fp4" name="is_operating_expenses_fp4" class="@error ('is_operating_expenses_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_operating_expenses_fp4'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_operating_expenses_fp4" name="is_operating_expenses_fp4" class="@error ('is_operating_expenses_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_operating_expenses_fp4') }}" >
                                                @endif


                                                @error("is_operating_expenses_fp4")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 5
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_operating_expenses_fp5" name="is_operating_expenses_fp5" class="@error ('is_operating_expenses_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_operating_expenses_fp5'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_operating_expenses_fp5" name="is_operating_expenses_fp5" class="@error ('is_operating_expenses_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_operating_expenses_fp5') }}" >
                                                @endif

                                                @error("is_operating_expenses_fp5")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div><hr>


                                    <div class="form-group m-form__group">
                                        <label class="col-md-3 col-form-label">
                                            <strong> Profit before tax: </strong>
                                        </label>
                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_profit_before_tax_py3" name="is_profit_before_tax_py3" class="@error ('is_profit_before_tax_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_profit_before_tax_py3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_profit_before_tax_py3" name="is_profit_before_tax_py3" class="@error ('is_profit_before_tax_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_profit_before_tax_py3') }}" >
                                                @endif

                                                @error("is_profit_before_tax_py3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_profit_before_tax_py2" name="is_profit_before_tax_py2" class="@error ('is_profit_before_tax_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_profit_before_tax_py2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_profit_before_tax_py2" name="is_profit_before_tax_py2" class="@error ('is_profit_before_tax_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_profit_before_tax_py2') }}" >
                                                @endif

                                                @error("is_profit_before_tax_py2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_profit_before_tax_py1" name="is_profit_before_tax_py1" class="@error ('is_profit_before_tax_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_profit_before_tax_py1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_profit_before_tax_py1" name="is_profit_before_tax_py1" class="@error ('is_profit_before_tax_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_profit_before_tax_py1') }}" >
                                                @endif

                                                @error("is_profit_before_tax_py1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3">
                                                <span class="m-form__help">
                                                    YTD
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_profit_before_tax_ytd" name="is_profit_before_tax_ytd" class="@error ('is_profit_before_tax_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_profit_before_tax_ytd'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_profit_before_tax_ytd" name="is_profit_before_tax_ytd" class="@error ('is_profit_before_tax_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_profit_before_tax_ytd') }}" >
                                                @endif

                                                @error("is_profit_before_tax_ytd")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_profit_before_tax_fp1" name="is_profit_before_tax_fp1" class="@error ('is_profit_before_tax_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_profit_before_tax_fp1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_profit_before_tax_fp1" name="is_profit_before_tax_fp1" class="@error ('is_profit_before_tax_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_profit_before_tax_fp1') }}" >
                                                @endif

                                                @error("is_profit_before_tax_fp1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_profit_before_tax_fp2" name="is_profit_before_tax_fp2" class="@error ('is_profit_before_tax_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_profit_before_tax_fp2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_profit_before_tax_fp2" name="is_profit_before_tax_fp2" class="@error ('is_profit_before_tax_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_profit_before_tax_fp2') }}" >
                                                @endif

                                                @error("is_profit_before_tax_fp2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_profit_before_tax_fp3" name="is_profit_before_tax_fp3" class="@error ('is_profit_before_tax_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_profit_before_tax_fp3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_profit_before_tax_fp3" name="is_profit_before_tax_fp3" class="@error ('is_profit_before_tax_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_profit_before_tax_fp3') }}" >
                                                @endif

                                                @error("is_profit_before_tax_fp3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 4
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_profit_before_tax_fp4" name="is_profit_before_tax_fp4" class="@error ('is_profit_before_tax_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_profit_before_tax_fp4'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_profit_before_tax_fp4" name="is_profit_before_tax_fp4" class="@error ('is_profit_before_tax_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_profit_before_tax_fp4') }}" >
                                                @endif

                                                @error("is_profit_before_tax_fp4")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 5
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_profit_before_tax_fp5" name="is_profit_before_tax_fp5" class="@error ('is_profit_before_tax_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_profit_before_tax_fp5'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_profit_before_tax_fp5" name="is_profit_before_tax_fp5" class="@error ('is_profit_before_tax_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_profit_before_tax_fp5') }}" >
                                                @endif

                                                @error("is_profit_before_tax_fp5")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div><hr>

                                    <div class="form-group m-form__group">
                                        <label class="col-md-3 col-form-label">
                                            <strong> Tax: </strong>
                                        </label>
                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_tax_py3" name="is_tax_py3" class="@error ('is_tax_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_tax_py3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_tax_py3" name="is_tax_py3" class="@error ('is_tax_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_tax_py3') }}" >
                                                @endif

                                                @error("is_tax_py3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_tax_py2" name="is_tax_py2" class="@error ('is_tax_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_tax_py2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_tax_py2" name="is_tax_py2" class="@error ('is_tax_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_tax_py2') }}" >
                                                @endif

                                                @error("is_tax_py2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_tax_py1" name="is_tax_py1" class="@error ('is_tax_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_tax_py1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_tax_py1" name="is_tax_py1" class="@error ('is_tax_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_tax_py1') }}" >
                                                @endif

                                                @error("is_tax_py1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3">
                                                <span class="m-form__help">
                                                    YTD
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_tax_ytd" name="is_tax_ytd" class="@error ('is_tax_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_tax_ytd'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_tax_ytd" name="is_tax_ytd" class="@error ('is_tax_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_tax_ytd') }}" >
                                                @endif

                                                @error("is_tax_ytd")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_tax_fp1" name="is_tax_fp1" class="@error ('is_tax_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_tax_fp1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_tax_fp1" name="is_tax_fp1" class="@error ('is_tax_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_tax_fp1') }}" >
                                                @endif

                                                @error("is_tax_fp1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_tax_fp2" name="is_tax_fp2" class="@error ('is_tax_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_tax_fp2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_tax_fp2" name="is_tax_fp2" class="@error ('is_tax_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_tax_fp2') }}" >
                                                @endif

                                                @error("is_tax_fp2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_tax_fp3" name="is_tax_fp3" class="@error ('is_tax_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_tax_fp3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_tax_fp3" name="is_tax_fp3" class="@error ('is_tax_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_tax_fp3') }}" >
                                                @endif

                                                @error("is_tax_fp3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 4
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_tax_fp4" name="is_tax_fp4" class="@error ('is_tax_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_tax_fp4'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_tax_fp4" name="is_tax_fp4" class="@error ('is_tax_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_tax_fp4') }}" >
                                                @endif

                                                @error("is_tax_fp4")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 5
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_tax_fp5" name="is_tax_fp5" class="@error ('is_tax_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_tax_fp5'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_tax_fp5" name="is_tax_fp5" class="@error ('is_tax_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_tax_fp5') }}" >
                                                @endif

                                                @error("is_tax_fp5")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div><hr>

                                    <div class="form-group m-form__group">
                                        <label class="col-md-3 col-form-label">
                                            <strong> Profit After Tax:</strong>
                                        </label>
                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_profit_after_tax_py3" name="is_profit_after_tax_py3" class="@error ('is_profit_after_tax_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_profit_after_tax_py3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_profit_after_tax_py3" name="is_profit_after_tax_py3" class="@error ('is_profit_after_tax_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_profit_after_tax_py3') }}" >
                                                @endif

                                                @error("is_profit_after_tax_py3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_profit_after_tax_py2" name="is_profit_after_tax_py2" class="@error ('is_profit_after_tax_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_profit_after_tax_py2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_profit_after_tax_py2" name="is_profit_after_tax_py2" class="@error ('is_profit_after_tax_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_profit_after_tax_py2') }}" >
                                                @endif

                                                @error("is_profit_after_tax_py2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_profit_after_tax_py1" name="is_profit_after_tax_py1" class="@error ('is_profit_after_tax_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_profit_after_tax_py1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_profit_after_tax_py1" name="is_profit_after_tax_py1" class="@error ('is_profit_after_tax_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_profit_after_tax_py1') }}" >
                                                @endif

                                                @error("is_profit_after_tax_py1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3">
                                                <span class="m-form__help">
                                                    YTD
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_profit_after_tax_ytd" name="is_profit_after_tax_ytd" class="@error ('is_profit_after_tax_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_profit_after_tax_ytd'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_profit_after_tax_ytd" name="is_profit_after_tax_ytd" class="@error ('is_profit_after_tax_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_profit_after_tax_ytd') }}" >
                                                @endif

                                                @error("is_profit_after_tax_ytd")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_profit_after_tax_fp1" name="is_profit_after_tax_fp1" class="@error ('is_profit_after_tax_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_profit_after_tax_fp1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_profit_after_tax_fp1" name="is_profit_after_tax_fp1" class="@error ('is_profit_after_tax_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_profit_after_tax_fp1') }}" >
                                                @endif

                                                @error("is_profit_after_tax_fp1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_profit_after_tax_fp2" name="is_profit_after_tax_fp2" class="@error ('is_profit_after_tax_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_profit_after_tax_fp2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_profit_after_tax_fp2" name="is_profit_after_tax_fp2" class="@error ('is_profit_after_tax_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_profit_after_tax_fp2') }}" >
                                                @endif

                                                @error("is_profit_after_tax_fp2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_profit_after_tax_fp3" name="is_profit_after_tax_fp3" class="@error ('is_profit_after_tax_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_profit_after_tax_fp3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_profit_after_tax_fp3" name="is_profit_after_tax_fp3" class="@error ('is_profit_after_tax_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_profit_after_tax_fp3') }}" >
                                                @endif

                                                @error("is_profit_after_tax_fp3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 4
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_profit_after_tax_fp4" name="is_profit_after_tax_fp4" class="@error ('is_profit_after_tax_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_profit_after_tax_fp4'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_profit_after_tax_fp4" name="is_profit_after_tax_fp4" class="@error ('is_profit_after_tax_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_profit_after_tax_fp4') }}" >
                                                @endif

                                                @error("is_profit_after_tax_fp4")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 5
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_profit_after_tax_fp5" name="is_profit_after_tax_fp5" class="@error ('is_profit_after_tax_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_profit_after_tax_fp5'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_profit_after_tax_fp5" name="is_profit_after_tax_fp5" class="@error ('is_profit_after_tax_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_profit_after_tax_fp5') }}" >
                                                @endif

                                                @error("is_profit_after_tax_fp5")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div><hr>


                                    <div class="form-group m-form__group">
                                        <label class="col-md-2 col-form-label">
                                            <strong> Depreciation: </strong>
                                        </label>
                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -3
                                                </span>
                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_depreciation_py3" name="is_depreciation_py3" class="@error ('is_depreciation_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_depreciation_py3'] }}" >
                                                    @endforeach
                                                @else

                                                    <input type="number" id="is_depreciation_py3" name="is_depreciation_py3" class="@error ('is_depreciation_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_depreciation_py3') }}" >
                                                @endif

                                                @error("is_depreciation_py3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -2
                                                </span>
                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_depreciation_py2" name="is_depreciation_py2" class="@error ('is_depreciation_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_depreciation_py2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_depreciation_py2" name="is_depreciation_py2" class="@error ('is_depreciation_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_depreciation_py2') }}" >
                                                @endif

                                                @error("is_depreciation_py2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -1
                                                </span>
                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_depreciation_py1" name="is_depreciation_py1" class="@error ('is_depreciation_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_depreciation_py1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_depreciation_py1" name="is_depreciation_py1" class="@error ('is_depreciation_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_depreciation_py1') }}" >
                                                @endif

                                                @error("is_depreciation_py1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3">
                                                <span class="m-form__help">
                                                    YTD
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_depreciation_ytd" name="is_depreciation_ytd" class="@error ('is_depreciation_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_depreciation_ytd'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_depreciation_ytd" name="is_depreciation_ytd" class="@error ('is_depreciation_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_depreciation_ytd') }}" >
                                                @endif

                                                @error("is_depreciation_ytd")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 1
                                                </span>
                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_depreciation_fp1" name="is_depreciation_fp1" class="@error ('is_depreciation_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_depreciation_fp1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_depreciation_fp1" name="is_depreciation_fp1" class="@error ('is_depreciation_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_depreciation_fp1') }}" >
                                                @endif

                                                @error("is_depreciation_fp1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_depreciation_fp2" name="is_depreciation_fp2" class="@error ('is_depreciation_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_depreciation_fp2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_depreciation_fp2" name="is_depreciation_fp2" class="@error ('is_depreciation_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_depreciation_fp2') }}" >
                                                @endif

                                                @error("is_depreciation_fp2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_depreciation_fp3" name="is_depreciation_fp3" class="@error ('is_depreciation_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_depreciation_fp3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_depreciation_fp3" name="is_depreciation_fp3" class="@error ('is_depreciation_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_depreciation_fp3') }}" >
                                                @endif

                                                @error("is_depreciation_fp3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 4
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_depreciation_fp4" name="is_depreciation_fp4" class="@error ('is_depreciation_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_depreciation_fp4'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_depreciation_fp4" name="is_depreciation_fp4" class="@error ('is_depreciation_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_depreciation_fp4') }}" >
                                                @endif

                                                @error("is_depreciation_fp4")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 5
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_depreciation_fp5" name="is_depreciation_fp5" class="@error ('is_depreciation_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_depreciation_fp5'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_depreciation_fp5" name="is_depreciation_fp5" class="@error ('is_depreciation_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_depreciation_fp5') }}" >
                                                @endif

                                                @error("is_depreciation_fp5")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div><hr>

                                    <div class="form-group m-form__group">
                                        <label class="col-md-3 col-form-label">
                                            <strong> Interest Expense: </strong>
                                        </label>
                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_interest_expense_py3" name="is_interest_expense_py3" class="@error ('is_interest_expense_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_interest_expense_py3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_interest_expense_py3" name="is_interest_expense_py3" class="@error ('is_interest_expense_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_interest_expense_py3') }}" >
                                                @endif

                                                @error("is_interest_expense_py3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_interest_expense_py2" name="is_interest_expense_py2" class="@error ('is_interest_expense_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_interest_expense_py2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_interest_expense_py2" name="is_interest_expense_py2" class="@error ('is_interest_expense_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_interest_expense_py2') }}" >
                                                @endif

                                                @error("is_interest_expense_py2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_interest_expense_py1" name="is_interest_expense_py1" class="@error ('is_interest_expense_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_interest_expense_py1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_interest_expense_py1" name="is_interest_expense_py1" class="@error ('is_interest_expense_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_interest_expense_py1') }}" >
                                                @endif

                                                @error("is_interest_expense_py1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3">
                                                <span class="m-form__help">
                                                    YTD
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_interest_expense_ytd" name="is_interest_expense_ytd" class="@error ('is_interest_expense_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_interest_expense_ytd'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_interest_expense_ytd" name="is_interest_expense_ytd" class="@error ('is_interest_expense_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_interest_expense_ytd') }}" >
                                                @endif

                                                @error("is_interest_expense_ytd")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_interest_expense_fp1" name="is_interest_expense_fp1" class="@error ('is_interest_expense_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_interest_expense_fp1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_interest_expense_fp1" name="is_interest_expense_fp1" class="@error ('is_interest_expense_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_interest_expense_fp1') }}" >
                                                @endif

                                                @error("is_interest_expense_fp1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_interest_expense_fp2" name="is_interest_expense_fp2" class="@error ('is_interest_expense_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_interest_expense_fp2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_interest_expense_fp2" name="is_interest_expense_fp2" class="@error ('is_interest_expense_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_interest_expense_fp2') }}" >
                                                @endif

                                                @error("is_interest_expense_fp2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_interest_expense_fp3" name="is_interest_expense_fp3" class="@error ('is_interest_expense_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_interest_expense_fp3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_interest_expense_fp3" name="is_interest_expense_fp3" class="@error ('is_interest_expense_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_interest_expense_fp3') }}" >
                                                @endif

                                                @error("is_interest_expense_fp3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 4
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_interest_expense_fp4" name="is_interest_expense_fp4" class="@error ('is_interest_expense_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_interest_expense_fp4'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_interest_expense_fp4" name="is_interest_expense_fp4" class="@error ('is_interest_expense_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_interest_expense_fp4') }}" >
                                                @endif

                                                @error("is_interest_expense_fp4")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 5
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="is_interest_expense_fp5" name="is_interest_expense_fp5" class="@error ('is_interest_expense_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['is_interest_expense_fp5'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="is_interest_expense_fp5" name="is_interest_expense_fp5" class="@error ('is_interest_expense_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('is_interest_expense_fp5') }}" >
                                                @endif

                                                @error("is_interest_expense_fp5")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div><br><br>

                                <div class="m-form__section mt-5">
                                    <div class="m-form__heading">
                                        <h5 class="m-form__heading-title">
                                            Balance Sheet (<strong> PY - Prior Year | FP - Forecast Period | YTD - Year To Date </strong>)
                                        </h5>
                                    </div>
                                </div>

                                <div class="m-form__section">
                                    <div class="form-group m-form__group">
                                        <label class="col-xl-2 col-lg-2 col-form-label">
                                            <strong> Fixed Assets: </strong>
                                        </label>
                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_fixed_assets_py3" name="bs_fixed_assets_py3" class="@error ('bs_fixed_assets_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_fixed_assets_py3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_fixed_assets_py3" name="bs_fixed_assets_py3" class="@error ('bs_fixed_assets_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_fixed_assets_py3') }}" >
                                                @endif


                                                @error("bs_fixed_assets_py3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_fixed_assets_py2" name="bs_fixed_assets_py2" class="@error ('bs_fixed_assets_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_fixed_assets_py2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_fixed_assets_py2" name="bs_fixed_assets_py2" class="@error ('bs_fixed_assets_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_fixed_assets_py2') }}" >
                                                @endif


                                                @error("bs_fixed_assets_py2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_fixed_assets_py1" name="bs_fixed_assets_py1" class="@error ('bs_fixed_assets_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_fixed_assets_py1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_fixed_assets_py1" name="bs_fixed_assets_py1" class="@error ('bs_fixed_assets_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_fixed_assets_py1') }}" >
                                                @endif


                                                @error("bs_fixed_assets_py1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3">
                                                <span class="m-form__help">
                                                    YTD
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_fixed_assets_ytd" name="bs_fixed_assets_ytd" class="@error ('bs_fixed_assets_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_fixed_assets_ytd'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_fixed_assets_ytd" name="bs_fixed_assets_ytd" class="@error ('bs_fixed_assets_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_fixed_assets_ytd') }}" >
                                                @endif

                                                @error("bs_fixed_assets_ytd")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_fixed_assets_fp1" name="bs_fixed_assets_fp1" class="@error ('bs_fixed_assets_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_fixed_assets_fp1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_fixed_assets_fp1" name="bs_fixed_assets_fp1" class="@error ('bs_fixed_assets_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_fixed_assets_fp1') }}" >
                                                @endif

                                                @error("bs_fixed_assets_fp1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_fixed_assets_fp2" name="bs_fixed_assets_fp2" class="@error ('bs_fixed_assets_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_fixed_assets_fp2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_fixed_assets_fp2" name="bs_fixed_assets_fp2" class="@error ('bs_fixed_assets_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_fixed_assets_fp2') }}" >
                                                @endif

                                                @error("bs_fixed_assets_fp2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_fixed_assets_fp3" name="bs_fixed_assets_fp3" class="@error ('bs_fixed_assets_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_fixed_assets_fp3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_fixed_assets_fp3" name="bs_fixed_assets_fp3" class="@error ('bs_fixed_assets_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_fixed_assets_fp3') }}" >
                                                @endif

                                                @error("bs_fixed_assets_fp3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 4
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_fixed_assets_fp4" name="bs_fixed_assets_fp4" class="@error ('bs_fixed_assets_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_fixed_assets_fp4'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_fixed_assets_fp4" name="bs_fixed_assets_fp4" class="@error ('bs_fixed_assets_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_fixed_assets_fp4') }}" >
                                                @endif

                                                @error("bs_fixed_assets_fp4")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 5
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_fixed_assets_fp5" name="bs_fixed_assets_fp5" class="@error ('bs_fixed_assets_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_fixed_assets_fp5'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_fixed_assets_fp5" name="bs_fixed_assets_fp5" class="@error ('bs_fixed_assets_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_fixed_assets_fp5') }}" >
                                                @endif

                                                @error("bs_fixed_assets_fp5")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div><hr>

                                <div class="m-form__section">
                                    <div class="form-group m-form__group">
                                        <label class="col-md-3 col-form-label">
                                            <strong> Current Assets: </strong>
                                        </label>
                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_current_assets_py3" name="bs_current_assets_py3" class="@error ('bs_current_assets_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_current_assets_py3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_current_assets_py3" name="bs_current_assets_py3" class="@error ('bs_current_assets_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_current_assets_py3') }}" >
                                                @endif


                                                @error("bs_current_assets_py3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_current_assets_py2" name="bs_current_assets_py2" class="@error ('bs_current_assets_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_current_assets_py2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_current_assets_py2" name="bs_current_assets_py2" class="@error ('bs_current_assets_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_current_assets_py2') }}" >
                                                @endif


                                                @error("bs_current_assets_py2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_current_assets_py1" name="bs_current_assets_py1" class="@error ('bs_current_assets_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_current_assets_py1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_current_assets_py1" name="bs_current_assets_py1" class="@error ('bs_current_assets_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_current_assets_py1') }}" >
                                                @endif


                                                @error("bs_current_assets_py1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3">
                                                <span class="m-form__help">
                                                    YTD
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_current_assets_ytd" name="bs_current_assets_ytd" class="@error ('bs_current_assets_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_current_assets_ytd'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_current_assets_ytd" name="bs_current_assets_ytd" class="@error ('bs_current_assets_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_current_assets_ytd') }}" >
                                                @endif

                                                @error("bs_current_assets_ytd")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_current_assets_fp1" name="bs_current_assets_fp1" class="@error ('bs_current_assets_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_current_assets_fp1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_current_assets_fp1" name="bs_current_assets_fp1" class="@error ('bs_current_assets_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_current_assets_fp1') }}" >
                                                @endif


                                                @error("bs_current_assets_fp1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_current_assets_fp2" name="bs_current_assets_fp2" class="@error ('bs_current_assets_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_current_assets_fp2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_current_assets_fp2" name="bs_current_assets_fp2" class="@error ('bs_current_assets_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_current_assets_fp2') }}" >
                                                @endif

                                                @error("bs_current_assets_fp2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_current_assets_fp3" name="bs_current_assets_fp3" class="@error ('bs_current_assets_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_current_assets_fp3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_current_assets_fp3" name="bs_current_assets_fp3" class="@error ('bs_current_assets_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_current_assets_fp3') }}" >
                                                @endif


                                                @error("bs_current_assets_fp3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 4
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_current_assets_fp4" name="bs_current_assets_fp4" class="@error ('bs_current_assets_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_current_assets_fp4'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_current_assets_fp4" name="bs_current_assets_fp4" class="@error ('bs_current_assets_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_current_assets_fp4') }}" >
                                                @endif


                                                @error("bs_current_assets_fp4")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 5
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_current_assets_fp5" name="bs_current_assets_fp5" class="@error ('bs_current_assets_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_current_assets_fp5'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_current_assets_fp5" name="bs_current_assets_fp5" class="@error ('bs_current_assets_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_current_assets_fp5') }}" >
                                                @endif


                                                @error("bs_current_assets_fp5")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div><hr>

                                <div class="m-form__section mt-5">
                                    <div class="form-group m-form__group">
                                        <label class="col-xl-2 col-lg-2 col-form-label">
                                            <strong> Total Assets: </strong>
                                        </label>
                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_total_assets_py3" name="bs_total_assets_py3" class="@error ('bs_total_assets_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_total_assets_py3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_total_assets_py3" name="bs_total_assets_py3" class="@error ('bs_total_assets_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_total_assets_py3') }}" >
                                                @endif

                                                @error("bs_total_assets_py3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_total_assets_py2" name="bs_total_assets_py2" class="@error ('bs_total_assets_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_total_assets_py2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_total_assets_py2" name="bs_total_assets_py2" class="@error ('bs_total_assets_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_total_assets_py2') }}" >
                                                @endif

                                                @error("bs_total_assets_py2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_total_assets_py1" name="bs_total_assets_py1" class="@error ('bs_total_assets_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_total_assets_py1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_total_assets_py1" name="bs_total_assets_py1" class="@error ('bs_total_assets_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_total_assets_py1') }}" >
                                                @endif

                                                @error("bs_total_assets_py1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3">
                                                <span class="m-form__help">
                                                    YTD
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_total_assets_ytd" name="bs_total_assets_ytd" class="@error ('bs_total_assets_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_total_assets_ytd'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_total_assets_ytd" name="bs_total_assets_ytd" class="@error ('bs_total_assets_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_total_assets_ytd') }}" >
                                                @endif

                                                @error("bs_total_assets_ytd")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_total_assets_fp1" name="bs_total_assets_fp1" class="@error ('bs_total_assets_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_total_assets_fp1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_total_assets_fp1" name="bs_total_assets_fp1" class="@error ('bs_total_assets_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_total_assets_fp1') }}" >
                                                @endif

                                                @error("bs_total_assets_fp1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_total_assets_fp2" name="bs_total_assets_fp2" class="@error ('bs_total_assets_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_total_assets_fp2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_total_assets_fp2" name="bs_total_assets_fp2" class="@error ('bs_total_assets_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_total_assets_fp2') }}" >
                                                @endif

                                                @error("bs_total_assets_fp2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_total_assets_fp3" name="bs_total_assets_fp3" class="@error ('bs_total_assets_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_total_assets_fp3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_total_assets_fp3" name="bs_total_assets_fp3" class="@error ('bs_total_assets_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_total_assets_fp3') }}" >
                                                @endif

                                                @error("bs_total_assets_fp3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 4
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_total_assets_fp4" name="bs_total_assets_fp4" class="@error ('bs_total_assets_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_total_assets_fp4'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_total_assets_fp4" name="bs_total_assets_fp4" class="@error ('bs_total_assets_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_total_assets_fp4') }}" >
                                                @endif

                                                @error("bs_total_assets_fp4")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 5
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_total_assets_fp5" name="bs_total_assets_fp5" class="@error ('bs_total_assets_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_total_assets_fp5'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_total_assets_fp5" name="bs_total_assets_fp5" class="@error ('bs_total_assets_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_total_assets_fp5') }}" >
                                                @endif

                                                @error("bs_total_assets_fp5")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div><hr>

                                <div class="m-form__section">
                                    <div class="form-group m-form__group">
                                        <label class="col-md-3 col-form-label">
                                            <strong> Long Term Liabilities: </strong>
                                        </label>
                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_longterm_liabilities_py3" name="bs_longterm_liabilities_py3" class="@error ('bs_longterm_liabilities_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_longterm_liabilities_py3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_longterm_liabilities_py3" name="bs_longterm_liabilities_py3" class="@error ('bs_longterm_liabilities_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_longterm_liabilities_py3') }}" >
                                                @endif

                                                @error("bs_longterm_liabilities_py3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_longterm_liabilities_py2" name="bs_longterm_liabilities_py2" class="@error ('bs_longterm_liabilities_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_longterm_liabilities_py2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_longterm_liabilities_py2" name="bs_longterm_liabilities_py2" class="@error ('bs_longterm_liabilities_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_longterm_liabilities_py2') }}" >
                                                @endif

                                                @error("bs_longterm_liabilities_py2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_longterm_liabilities_py1" name="bs_longterm_liabilities_py1" class="@error ('bs_longterm_liabilities_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_longterm_liabilities_py1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_longterm_liabilities_py1" name="bs_longterm_liabilities_py1" class="@error ('bs_longterm_liabilities_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_longterm_liabilities_py1') }}" >
                                                @endif

                                                @error("bs_longterm_liabilities_py1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3">
                                                <span class="m-form__help">
                                                    YTD
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_longterm_liabilities_ytd" name="bs_longterm_liabilities_ytd" class="@error ('bs_longterm_liabilities_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_longterm_liabilities_ytd'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_longterm_liabilities_ytd" name="bs_longterm_liabilities_ytd" class="@error ('bs_longterm_liabilities_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_longterm_liabilities_ytd') }}" >
                                                @endif

                                                @error("bs_longterm_liabilities_ytd")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_longterm_liabilities_fp1" name="bs_longterm_liabilities_fp1" class="@error ('bs_longterm_liabilities_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ $finance['bs_longterm_liabilities_fp1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_longterm_liabilities_fp1" name="bs_longterm_liabilities_fp1" class="@error ('bs_longterm_liabilities_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ old('bs_longterm_liabilities_fp1') }}" >
                                                @endif

                                                @error("bs_longterm_liabilities_fp1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_longterm_liabilities_fp2" name="bs_longterm_liabilities_fp2" class="@error ('bs_longterm_liabilities_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ $finance['bs_longterm_liabilities_fp2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_longterm_liabilities_fp2" name="bs_longterm_liabilities_fp2" class="@error ('bs_longterm_liabilities_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ old('bs_longterm_liabilities_fp2') }}" >
                                                @endif

                                                @error("bs_longterm_liabilities_fp2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_longterm_liabilities_fp3" name="bs_longterm_liabilities_fp3" class="@error ('bs_longterm_liabilities_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ $finance['bs_longterm_liabilities_fp3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_longterm_liabilities_fp3" name="bs_longterm_liabilities_fp3" class="@error ('bs_longterm_liabilities_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ old('bs_longterm_liabilities_fp3') }}" >
                                                @endif

                                                @error("bs_longterm_liabilities_fp3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 4
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_longterm_liabilities_fp4" name="bs_longterm_liabilities_fp4" class="@error ('bs_longterm_liabilities_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ $finance['bs_longterm_liabilities_fp4'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_longterm_liabilities_fp4" name="bs_longterm_liabilities_fp4" class="@error ('bs_longterm_liabilities_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ old('bs_longterm_liabilities_fp4') }}" >
                                                @endif

                                                @error("bs_longterm_liabilities_fp4")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 5
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_longterm_liabilities_fp5" name="bs_longterm_liabilities_fp5" class="@error ('bs_longterm_liabilities_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ $finance['bs_longterm_liabilities_fp5'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_longterm_liabilities_fp5" name="bs_longterm_liabilities_fp5" class="@error ('bs_longterm_liabilities_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ old('bs_longterm_liabilities_fp5') }}" >
                                                @endif

                                                @error("bs_longterm_liabilities_fp5")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div><hr>

                                <div class="m-form__section">
                                    <div class="form-group m-form__group">
                                        <label class="col-md-3 col-form-label">
                                            <strong> Current Liabilities: </strong>
                                        </label>
                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_current_liabilities_py3" name="bs_current_liabilities_py3" class="@error ('bs_current_liabilities_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ $finance['bs_current_liabilities_py3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_current_liabilities_py3" name="bs_current_liabilities_py3" class="@error ('bs_current_liabilities_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ old('bs_current_liabilities_py3') }}" >
                                                @endif

                                                @error("bs_current_liabilities_py3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_current_liabilities_py2" name="bs_current_liabilities_py2" class="@error ('bs_current_liabilities_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ $finance['bs_current_liabilities_py2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_current_liabilities_py2" name="bs_current_liabilities_py2" class="@error ('bs_current_liabilities_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ old('bs_current_liabilities_py2') }}" >
                                                @endif

                                                @error("bs_current_liabilities_py2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_current_liabilities_py1" name="bs_current_liabilities_py1" class="@error ('bs_current_liabilities_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ $finance['bs_current_liabilities_py1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_current_liabilities_py1" name="bs_current_liabilities_py1" class="@error ('bs_current_liabilities_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ old('bs_current_liabilities_py1') }}" >
                                                @endif

                                                @error("bs_current_liabilities_py1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3">
                                                <span class="m-form__help">
                                                    YTD
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_current_liabilities_ytd" name="bs_current_liabilities_ytd" class="@error ('bs_current_liabilities_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ $finance['bs_current_liabilities_ytd'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_current_liabilities_ytd" name="bs_current_liabilities_ytd" class="@error ('bs_current_liabilities_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ old('bs_current_liabilities_ytd') }}" >
                                                @endif

                                                @error("bs_current_liabilities_ytd")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_current_liabilities_fp1" name="bs_current_liabilities_fp1" class="@error ('bs_current_liabilities_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ $finance['bs_current_liabilities_fp1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_current_liabilities_fp1" name="bs_current_liabilities_fp1" class="@error ('bs_current_liabilities_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ old('bs_current_liabilities_fp1') }}" >
                                                @endif

                                                @error("bs_current_liabilities_fp1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_current_liabilities_fp2" name="bs_current_liabilities_fp2" class="@error ('bs_current_liabilities_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ $finance['bs_current_liabilities_fp2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_current_liabilities_fp2" name="bs_current_liabilities_fp2" class="@error ('bs_current_liabilities_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ old('bs_current_liabilities_fp2') }}" >
                                                @endif

                                                @error("bs_current_liabilities_fp2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_current_liabilities_fp3" name="bs_current_liabilities_fp3" class="@error ('bs_current_liabilities_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ $finance['bs_current_liabilities_fp3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_current_liabilities_fp3" name="bs_current_liabilities_fp3" class="@error ('bs_current_liabilities_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ old('bs_current_liabilities_fp3') }}" >
                                                @endif

                                                @error("bs_current_liabilities_fp3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 4
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_current_liabilities_fp4" name="bs_current_liabilities_fp4" class="@error ('bs_current_liabilities_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ $finance['bs_current_liabilities_fp4'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_current_liabilities_fp4" name="bs_current_liabilities_fp4" class="@error ('bs_current_liabilities_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ old('bs_current_liabilities_fp4') }}" >
                                                @endif

                                                @error("bs_current_liabilities_fp4")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 5
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_current_liabilities_fp5" name="bs_current_liabilities_fp5" class="@error ('bs_current_liabilities_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ $finance['bs_current_liabilities_fp5'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_current_liabilities_fp5" name="bs_current_liabilities_fp5" class="@error ('bs_current_liabilities_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ old('bs_current_liabilities_fp5') }}" >
                                                @endif

                                                @error("bs_current_liabilities_fp5")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div><hr>


                                <div class="m-form__section">
                                    <div class="form-group m-form__group">
                                        <label class="col-md-3 col-form-label">
                                            <strong> Total Liabilities: </strong>
                                        </label>
                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_total_liabilities_py3" name="bs_total_liabilities_py3" class="@error ('bs_total_liabilities_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ $finance['bs_total_liabilities_py3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_total_liabilities_py3" name="bs_total_liabilities_py3" class="@error ('bs_total_liabilities_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ old('bs_total_liabilities_py3') }}" >
                                                @endif

                                                @error("bs_total_liabilities_py3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_total_liabilities_py2" name="bs_total_liabilities_py2" class="@error ('bs_total_liabilities_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ $finance['bs_total_liabilities_py2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_total_liabilities_py2" name="bs_total_liabilities_py2" class="@error ('bs_total_liabilities_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ old('bs_total_liabilities_py2') }}" >
                                                @endif

                                                @error("bs_total_liabilities_py2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_total_liabilities_py1" name="bs_total_liabilities_py1" class="@error ('bs_total_liabilities_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ $finance['bs_total_liabilities_py1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_total_liabilities_py1" name="bs_total_liabilities_py1" class="@error ('bs_total_liabilities_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ old('bs_total_liabilities_py1') }}" >
                                                @endif

                                                @error("bs_total_liabilities_py1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3">
                                                <span class="m-form__help">
                                                    YTD
                                                </span>
                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_total_liabilities_ytd" name="bs_total_liabilities_ytd" class="@error ('bs_total_liabilities_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ $finance['bs_total_liabilities_ytd'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_total_liabilities_ytd" name="bs_total_liabilities_ytd" class="@error ('bs_total_liabilities_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placehoder="" value="{{ old('bs_total_liabilities_ytd') }}" >
                                                @endif

                                                @error("bs_total_liabilities_ytd")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 1
                                                </span>
                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_total_liabilities_fp1" name="bs_total_liabilities_fp1" class="@error ('bs_total_liabilities_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_total_liabilities_fp1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_total_liabilities_fp1" name="bs_total_liabilities_fp1" class="@error ('bs_total_liabilities_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_total_liabilities_fp1') }}" >
                                                @endif

                                                @error("bs_total_liabilities_fp1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_total_liabilities_fp2" name="bs_total_liabilities_fp2" class="@error ('bs_total_liabilities_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_total_liabilities_fp2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_total_liabilities_fp2" name="bs_total_liabilities_fp2" class="@error ('bs_total_liabilities_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_total_liabilities_fp2') }}" >
                                                @endif

                                                @error("bs_total_liabilities_fp2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_total_liabilities_fp3" name="bs_total_liabilities_fp3" class="@error ('bs_total_liabilities_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_total_liabilities_fp3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_total_liabilities_fp3" name="bs_total_liabilities_fp3" class="@error ('bs_total_liabilities_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_total_liabilities_fp3') }}" >
                                                @endif

                                                @error("bs_total_liabilities_fp3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 4
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_total_liabilities_fp4" name="bs_total_liabilities_fp4" class="@error ('bs_total_liabilities_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_total_liabilities_fp4'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_total_liabilities_fp4" name="bs_total_liabilities_fp4" class="@error ('bs_total_liabilities_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_total_liabilities_fp4') }}" >
                                                @endif

                                                @error("bs_total_liabilities_fp4")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 5
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_total_liabilities_fp5" name="bs_total_liabilities_fp5" class="@error ('bs_total_liabilities_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_total_liabilities_fp5'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_total_liabilities_fp5" name="bs_total_liabilities_fp5" class="@error ('bs_total_liabilities_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_total_liabilities_fp5') }}" >
                                                @endif

                                                @error("bs_total_liabilities_fp5")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div><hr>

                                <div class="m-form__section">
                                    <div class="form-group m-form__group">
                                        <label class="col-md-2 col-form-label">
                                            <strong> Equity: </strong>
                                        </label>
                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -3
                                                </span>
                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_equity_py3" name="bs_equity_py3" class="@error ('bs_equity_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_equity_py3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_equity_py3" name="bs_equity_py3" class="@error ('bs_equity_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_equity_py3') }}" >
                                                @endif

                                                @error("bs_equity_py3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -2
                                                </span>
                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_equity_py2" name="bs_equity_py2" class="@error ('bs_equity_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_equity_py2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_equity_py2" name="bs_equity_py2" class="@error ('bs_equity_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_equity_py2') }}" >
                                                @endif

                                                @error("bs_equity_py2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -1
                                                </span>
                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_equity_py1" name="bs_equity_py1" class="@error ('bs_equity_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_equity_py1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_equity_py1" name="bs_equity_py1" class="@error ('bs_equity_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_equity_py1') }}" >
                                                @endif

                                                @error("bs_equity_py1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3">
                                                <span class="m-form__help">
                                                    YTD
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_equity_ytd" name="bs_equity_ytd" class="@error ('bs_equity_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_equity_ytd'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_equity_ytd" name="bs_equity_ytd" class="@error ('bs_equity_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_equity_ytd') }}" >
                                                @endif

                                                @error("bs_equity_ytd")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_equity_fp1" name="bs_equity_fp1" class="@error ('bs_equity_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_equity_fp1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_equity_fp1" name="bs_equity_fp1" class="@error ('bs_equity_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_equity_fp1') }}" >
                                                @endif

                                                @error("bs_equity_fp1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_equity_fp2" name="bs_equity_fp2" class="@error ('bs_equity_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_equity_fp2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_equity_fp2" name="bs_equity_fp2" class="@error ('bs_equity_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_equity_fp2') }}" >
                                                @endif

                                                @error("bs_equity_fp2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_equity_fp3" name="bs_equity_fp3" class="@error ('bs_equity_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_equity_fp3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_equity_fp3" name="bs_equity_fp3" class="@error ('bs_equity_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_equity_fp3') }}" >
                                                @endif

                                                @error("bs_equity_fp3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 4
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_equity_fp4" name="bs_equity_fp4" class="@error ('bs_equity_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_equity_fp4'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_equity_fp4" name="bs_equity_fp4" class="@error ('bs_equity_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_equity_fp4') }}" >
                                                @endif

                                                @error("bs_equity_fp4")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 5
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_equity_fp5" name="bs_equity_fp5" class="@error ('bs_equity_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_equity_fp5'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_equity_fp5" name="bs_equity_fp5" class="@error ('bs_equity_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_equity_fp5') }}" >
                                                @endif

                                                @error("bs_equity_fp5")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div><hr>

                                <div class="m-form__section">
                                    <div class="form-group m-form__group">
                                        <label class="col-md-3 col-form-label">
                                            <strong> Shareholder Loans: </strong>
                                        </label>
                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_shareholder_loans_py3" name="bs_shareholder_loans_py3" class="@error ('bs_shareholder_loans_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_shareholder_loans_py3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_shareholder_loans_py3" name="bs_shareholder_loans_py3" class="@error ('bs_shareholder_loans_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_shareholder_loans_py3') }}" >
                                                @endif

                                                @error("bs_shareholder_loans_py3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_shareholder_loans_py2" name="bs_shareholder_loans_py2" class="@error ('bs_shareholder_loans_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_shareholder_loans_py2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_shareholder_loans_py2" name="bs_shareholder_loans_py2" class="@error ('bs_shareholder_loans_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_shareholder_loans_py2') }}" >
                                                @endif

                                                @error("bs_shareholder_loans_py2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_shareholder_loans_py1" name="bs_shareholder_loans_py1" class="@error ('bs_shareholder_loans_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_shareholder_loans_py1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_shareholder_loans_py1" name="bs_shareholder_loans_py1" class="@error ('bs_shareholder_loans_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_shareholder_loans_py1') }}" >
                                                @endif

                                                @error("bs_shareholder_loans_py1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3">
                                                <span class="m-form__help">
                                                    YTD
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_shareholder_loans_ytd" name="bs_shareholder_loans_ytd" class="@error ('bs_shareholder_loans_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_shareholder_loans_ytd'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_shareholder_loans_ytd" name="bs_shareholder_loans_ytd" class="@error ('bs_shareholder_loans_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_shareholder_loans_ytd') }}" >
                                                @endif

                                                @error("bs_shareholder_loans_ytd")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_shareholder_loans_fp1" name="bs_shareholder_loans_fp1" class="@error ('bs_shareholder_loans_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_shareholder_loans_fp1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_shareholder_loans_fp1" name="bs_shareholder_loans_fp1" class="@error ('bs_shareholder_loans_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_shareholder_loans_fp1') }}" >
                                                @endif

                                                @error("bs_shareholder_loans_fp1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_shareholder_loans_fp2" name="bs_shareholder_loans_fp2" class="@error ('bs_shareholder_loans_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_shareholder_loans_fp2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_shareholder_loans_fp2" name="bs_shareholder_loans_fp2" class="@error ('bs_shareholder_loans_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_shareholder_loans_fp2') }}" >
                                                @endif

                                                @error("bs_shareholder_loans_fp2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_shareholder_loans_fp3" name="bs_shareholder_loans_fp3" class="@error ('bs_shareholder_loans_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_shareholder_loans_fp3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_shareholder_loans_fp3" name="bs_shareholder_loans_fp3" class="@error ('bs_shareholder_loans_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_shareholder_loans_fp3') }}" >
                                                @endif

                                                @error("bs_shareholder_loans_fp3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 4
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_shareholder_loans_fp4" name="bs_shareholder_loans_fp4" class="@error ('bs_shareholder_loans_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_shareholder_loans_fp4'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_shareholder_loans_fp4" name="bs_shareholder_loans_fp4" class="@error ('bs_shareholder_loans_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_shareholder_loans_fp4') }}" >
                                                @endif

                                                @error("bs_shareholder_loans_fp4")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 5
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_shareholder_loans_fp5" name="bs_shareholder_loans_fp5" class="@error ('bs_shareholder_loans_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_shareholder_loans_fp5'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_shareholder_loans_fp5" name="bs_shareholder_loans_fp5" class="@error ('bs_shareholder_loans_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_shareholder_loans_fp5') }}" >
                                                @endif

                                                @error("bs_shareholder_loans_fp5")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div><hr>

                                <div class="m-form__section">
                                    <div class="form-group m-form__group">
                                        <label class="col-md-3 col-form-label">
                                            <strong> Capital Expenditure: </strong>
                                        </label>
                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_capital_expenditure_py3" name="bs_capital_expenditure_py3" class="@error ('bs_capital_expenditure_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_capital_expenditure_py3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_capital_expenditure_py3" name="bs_capital_expenditure_py3" class="@error ('bs_capital_expenditure_py3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_capital_expenditure_py3') }}" >
                                                @endif

                                                @error("bs_capital_expenditure_py3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_capital_expenditure_py2" name="bs_capital_expenditure_py2" class="@error ('bs_capital_expenditure_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_capital_expenditure_py2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_capital_expenditure_py2" name="bs_capital_expenditure_py2" class="@error ('bs_capital_expenditure_py2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_capital_expenditure_py2') }}" >
                                                @endif

                                                @error("bs_capital_expenditure_py2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-3 col-lg-3 ">
                                                <span class="m-form__help">
                                                    PY -1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_capital_expenditure_py1" name="bs_capital_expenditure_py1" class="@error ('bs_capital_expenditure_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_capital_expenditure_py1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_capital_expenditure_py1" name="bs_capital_expenditure_py1" class="@error ('bs_capital_expenditure_py1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_capital_expenditure_py1') }}" >
                                                @endif

                                                @error("bs_capital_expenditure_py1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-3 col-lg-3">
                                                <span class="m-form__help">
                                                    YTD
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_capital_expenditure_ytd" name="bs_capital_expenditure_ytd" class="@error ('bs_capital_expenditure_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_capital_expenditure_ytd'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_capital_expenditure_ytd" name="bs_capital_expenditure_ytd" class="@error ('bs_capital_expenditure_ytd') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_capital_expenditure_ytd') }}" >
                                                @endif

                                                @error("bs_capital_expenditure_ytd")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>


                                        <div class="form-group m-form__group row pb-1 pt-1">
                                            <div class="col-md-1"></div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 1
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_capital_expenditure_fp1" name="bs_capital_expenditure_fp1" class="@error ('bs_capital_expenditure_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_capital_expenditure_fp1'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_capital_expenditure_fp1" name="bs_capital_expenditure_fp1" class="@error ('bs_capital_expenditure_fp1') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_capital_expenditure_fp1') }}" >
                                                @endif

                                                @error("bs_capital_expenditure_fp1")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 2
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_capital_expenditure_fp2" name="bs_capital_expenditure_fp2" class="@error ('bs_capital_expenditure_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_capital_expenditure_fp2'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_capital_expenditure_fp2" name="bs_capital_expenditure_fp2" class="@error ('bs_capital_expenditure_fp2') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_capital_expenditure_fp2') }}" >
                                                @endif

                                                @error("bs_capital_expenditure_fp2")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 3
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_capital_expenditure_fp3" name="bs_capital_expenditure_fp3" class="@error ('bs_capital_expenditure_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_capital_expenditure_fp3'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_capital_expenditure_fp3" name="bs_capital_expenditure_fp3" class="@error ('bs_capital_expenditure_fp3') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_capital_expenditure_fp3') }}" >
                                                @endif

                                                @error("bs_capital_expenditure_fp3")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 4
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_capital_expenditure_fp4" name="bs_capital_expenditure_fp4" class="@error ('bs_capital_expenditure_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_capital_expenditure_fp4'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_capital_expenditure_fp4" name="bs_capital_expenditure_fp4" class="@error ('bs_capital_expenditure_fp4') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_capital_expenditure_fp4') }}" >
                                                @endif

                                                @error("bs_capital_expenditure_fp4")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-xl-2 col-lg-2">
                                                <span class="m-form__help">
                                                    FP 5
                                                </span>

                                                @if (Session::has('session_financials'))
                                                    @foreach ($session_financials as $finance)
                                                        <input type="number" id="bs_capital_expenditure_fp5" name="bs_capital_expenditure_fp5" class="@error ('bs_capital_expenditure_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ $finance['bs_capital_expenditure_fp5'] }}" >
                                                    @endforeach
                                                @else
                                                    <input type="number" id="bs_capital_expenditure_fp5" name="bs_capital_expenditure_fp5" class="@error ('bs_capital_expenditure_fp5') is-invalid @enderror form-control m-input rounded-0 font-weight-bold pt-2 pb-2" placeholder="" value="{{ old('bs_capital_expenditure_fp5') }}" >
                                                @endif

                                                @error("bs_capital_expenditure_fp5")
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    @foreach ($form_compliants as $comp)
                                        <input type="text" name="company_name" id="" value="{{ $comp['company_name'] }}" hidden>
                                        <input type="text" name="registration_no" id="" value="{{ $comp['registration_no'] }}" hidden>
                                        <input type="text" name="tax_registration_no" id="" value="{{ $comp['tax_registration_no'] }}" hidden>
                                        <input type="text" name="vat_registration_no" id="" value="{{ $comp['vat_registration_no'] }}" hidden>
                                        <input type="text" name="contact_person" id="" value="{{ $comp['contact_person'] }}" hidden>
                                        <input type="text" name="contact_person_email" id="" value="{{ $comp['contact_person_email'] }}" hidden>
                                        <input type="text" name="contact_person_mobile" id="" value="{{ $comp['contact_person_mobile'] }}" hidden>
                                    @endforeach

                                    @foreach ($form_applicants as $item)
                                        <input type="text" name="firstname" id="" value="{{ $item['firstname'] }}" hidden>
                                        <input type="text" name="lastname" id="" value="{{ $item['lastname'] }}" hidden>
                                        <input type="text" name="race" id="" value="{{ $item['race'] }}" hidden>
                                        <input type="text" name="nationality" id="" value="{{ $item['nationality'] }}" hidden>
                                        <input type="text" name="email" id="" value="{{ $item['email'] }}" hidden>
                                        <input type="text" name="phone" id="" value="{{ $item['phone'] }}" hidden>
                                        <input type="text" name="business_status" id="" value="{{ $item['business_status'] }}" hidden>
                                    @endforeach

                                    @foreach ($form_backgrounds as $back)
                                        <input type="text" name="shareholder_name" id="" value="{{ $back['shareholder_name'] }}" hidden>
                                        <input type="text" name="shareholder_id" id="" value="{{ $back['shareholder_id'] }}" hidden>
                                        <input type="text" name="entity_name" id="" value="{{ $back['entity_name'] }}" hidden>
                                        <input type="text" name="entity_registration_number" id="" value="{{ $back['entity_registration_number'] }}" hidden>
                                        <input type="text" name="business_status" id="" value="{{ $back['business_status'] }}" hidden>
                                        <input type="text" name="individual_shareholder_percentage" id="" value="{{ $back['individual_shareholder_percentage'] }}" hidden>
                                        <input type="text" name="entity_shareholder_percentage" id="" value="{{ $back['entity_shareholder_percentage'] }}" hidden>
                                        <input type="text" name="management_company_name" id="" value="{{ $back['management_company_name'] }}" hidden>
                                        <input type="text" name="management_registration_number" id="" value="{{ $back['management_registration_number'] }}" hidden>
                                        <input type="text" name="management_company_position" id="" value="{{ $back['management_company_position'] }}" hidden>
                                        <input type="text" name="permanent_staff_no" id="" value="{{ $back['permanent_staff_no'] }}" hidden>
                                        <input type="text" name="temporary_staff_no" id="" value="{{ $back['temporary_staff_no'] }}" hidden>
                                        <input type="text" name="business_location_province" id="" value="{{ $back['business_location_province'] }}" hidden>
                                        <input type="text" name="business_nearest_city_location" id="" value="{{ $back['business_nearest_city_location'] }}" hidden>
                                        <input type="text" name="project_actual_location_area" id="" value="{{ $back['project_actual_location_area'] }}" hidden>
                                        <input type="text" name="business_description" id="" value="{{ $back['business_description'] }}" hidden>
                                        <input type="text" name="business_goto_strategy" id="" value="{{ $back['business_goto_strategy'] }}" hidden>
                                        <input type="text" name="business_competitive_advantage" id="" value="{{ $back['business_competitive_advantage'] }}" hidden>
                                        <input type="text" name="business_areas_of_improvement" id="" value="{{ $back['business_areas_of_improvement'] }}" hidden>
                                        <input type="text" name="business_strategic_opportunities" id="" value="{{ $back['business_strategic_opportunities'] }}" hidden>
                                        <input type="text" name="business_financial_year_end" id="" value="{{ $back['business_financial_year_end'] }}" hidden>
                                        <input type="text" name="business_accountant_name" id="" value="{{ $back['business_accountant_name'] }}" hidden>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group row pt-3">
                                <div class="col-md-8">
                                    <button type="submit" name="submit" class="btn btn-primary font-weight-bold pr-5 pl-5 mr-3">{{ __('Submit') }}</button>

                                    <button type="submit" name="save" class="btn btn-outline-primary font-weight-bold pr-5 pl-5">
                                        {{ __('Save for Later') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
