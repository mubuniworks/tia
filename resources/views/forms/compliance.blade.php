@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                @if (session('success_message'))
                    <div class="alert alert-success font-weight-bold">{{ session('success_message') }}</div>
                @elseif (session('error_message'))
                    <div class="alert alert-danger font-weight-bold">{{ session('error_message') }}</div>
                @elseif (session('warning_message'))
                    <div class="alert alert-warning font-weight-bold">{{ session('warning_message') }}</div>
                @elseif (session('info_message'))
                    <div class="alert alert-info font-weight-bold">{{ session('info_message') }}</div>
                @else
                @endif

                <div class="card mt-5">
                    <div class="card-header bg-primary text-white font-weight-bold text-uppercase rounded-0 pt-3 pb-3">{{ __('BUSINESS COMPLIANCE FORM') }}</div>
                    <div class="card-body">
                        <form method="POST" action="{{ url('compliance-register') }}" class="pr-5 pl-5" autocomplete="off">
                            @csrf
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="company_name" class="col-form-label font-weight-bold">{{ __('Company Name') }}</label>

                                    @if (Session::has('session_compliants'))
                                        @foreach ($session_compliants as $compliant)
                                            <input id="company_name" type="text" class="form-control rounded-0 font-weight-bold pt-2 pb-2 @error('company_name') is-invalid @enderror" name="company_name" value="{{ $compliant['company_name'] }}" autofocus>
                                            <small class="text-muted">enter company name per CIPC</small>
                                        @endforeach
                                    @else
                                        <input id="company_name" type="text" class="form-control rounded-0 font-weight-bold pt-2 pb-2 @error('company_name') is-invalid @enderror" name="company_name" value="{{ old('company_name') }}" autofocus>
                                        <small class="text-muted">enter company name per CIPC</small>
                                    @endif

                                    @error('company_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="registration_no" class="col-form-label font-weight-bold">{{ __('Registration Number') }}</label>

                                    @if (Session::has('session_compliants'))
                                        @foreach ($session_compliants as $compliant)
                                            <input id="registration_no" type="text" class="form-control rounded-0 font-weight-bold pt-2 pb-2 @error('registration_no') is-invalid @enderror" name="registration_no" value="{{ $compliant['registration_no'] }}" autofocus>
                                        @endforeach
                                    @else
                                        <input id="registration_no" type="text" class="form-control rounded-0 font-weight-bold pt-2 pb-2 @error('registration_no') is-invalid @enderror" name="registration_no" value="{{ old('registration_no') }}">
                                        <small class="text-muted">unique company registration number per CIPC</small>
                                    @endif

                                    @error('registration_no')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="tax_registration_no" class="col-form-label font-weight-bold">{{ __('Tax Registration Number') }}</label>

                                    @if (Session::has('session_compliants'))
                                        @foreach ($session_compliants as $compliant)
                                            <input id="tax_registration_no" type="text" class="form-control rounded-0 font-weight-bold pt-2 pb-2 @error('tax_registration_no') is-invalid @enderror" name="tax_registration_no" value="{{ $compliant['tax_registration_no'] }}" autofocus>
                                        @endforeach
                                    @else
                                        <input id="tax_registration_no" type="text" class="form-control rounded-0 font-weight-bold pt-2 pb-2 @error('tax_registration_no') is-invalid @enderror" name="tax_registration_no" value="{{ old('tax_registration_no') }}" >
                                        <small class="text-muted">unique tax registration number per CIPC</small>
                                    @endif

                                    @error('tax_registration_no')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="col-md-6">
                                    <label for="vat_registration_no" class="col-form-label font-weight-bold">{{ __('VAT Registration Number') }}</label>

                                    @if (Session::has('session_compliants'))
                                        @foreach ($session_compliants as $compliant)
                                            <input id="vat_registration_no" type="text" class="form-control rounded-0 font-weight-bold pt-2 pb-2 @error('vat_registration_no') is-invalid @enderror" name="vat_registration_no" value="{{ $compliant['vat_registration_no'] }}" autofocus>
                                        @endforeach
                                    @else
                                        <input id="vat_registration_no" type="text" class="form-control rounded-0 font-weight-bold pt-2 pb-2 @error('vat_registration_no') is-invalid @enderror" name="vat_registration_no" value="{{ old('vat_registration_no') }}">
                                        <small class="text-muted">unique VAT registration number per CIPC</small>
                                    @endif

                                    @error('vat_registration_no')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="contact_person" class="col-form-label font-weight-bold">{{ __('Contact Person') }}</label>

                                    @if (Session::has('session_compliants'))
                                        @foreach ($session_compliants as $compliant)
                                            <input id="contact_person" type="text" class="form-control rounded-0 font-weight-bold pt-2 pb-2 @error('contact_person') is-invalid @enderror" name="contact_person" value="{{ $compliant['contact_person'] }}" autofocus>
                                        @endforeach
                                    @else
                                        <input id="contact_person" type="contact_person" class="form-control rounded-0 font-weight-bold pt-2 pb-2 @error('contact_person') is-invalid @enderror" name="contact_person" value="{{ old('contact_person') }}">
                                        <small class="text-muted">name of the person that should be contacted with respect to the application</small>
                                    @endif

                                    @error('contact_person')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                </div>

                                <div class="col-md-6">
                                    <label for="contact_person_email" class="col-form-label font-weight-bold">{{ __('Contact Person Email') }}</label>

                                    @if (Session::has('session_compliants'))
                                        @foreach ($session_compliants as $compliant)
                                            <input id="contact_person_email" type="text" class="form-control rounded-0 font-weight-bold pt-2 pb-2 @error('contact_person_email') is-invalid @enderror" name="contact_person_email" value="{{ $compliant['contact_person_email'] }}" autofocus>
                                        @endforeach
                                    @else
                                        <input id="contact_person_email" type="email" class="form-control rounded-0 font-weight-bold pt-2 pb-2 @error('contact_person_email') is-invalid @enderror" name="contact_person_email" value="{{ old('contact_person_email') }}">
                                        <small class="text-muted">contact person email address</small>
                                    @endif

                                    @error('contact_person_email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="contact_person_mobile" class="col-form-label font-weight-bold">{{ __('Contact Details Mobile') }}</label>

                                    @if (Session::has('session_compliants'))
                                        @foreach ($session_compliants as $compliant)
                                            <input id="contact_person_mobile" type="text" class="form-control rounded-0 font-weight-bold pt-2 pb-2 @error('contact_person_mobile') is-invalid @enderror" name="contact_person_mobile" value="{{ $compliant['contact_person_mobile'] }}" autofocus>
                                        @endforeach
                                    @else
                                        <input id="contact_person_mobile" type="text" class="form-control rounded-0 font-weight-bold pt-2 pb-2 @error('contact_person_mobile') is-invalid @enderror" name="contact_person_mobile" value="{{ old('contact_person_mobile') }}">
                                        <small class="text-muted">contact details mobile phone</small>
                                    @endif

                                    @error('vat_contact_person_mobile')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                </div>

                                <div class="col-md-6">
                                    @foreach ($form_applicants as $item)
                                        <input type="text" name="firstname" id="" value="{{ $item['firstname'] }}" hidden>
                                        <input type="text" name="lastname" id="" value="{{ $item['lastname'] }}" hidden>
                                        <input type="text" name="race" id="" value="{{ $item['race'] }}" hidden>
                                        <input type="text" name="nationality" id="" value="{{ $item['nationality'] }}" hidden>
                                        <input type="text" name="email" id="" value="{{ $item['email'] }}" hidden>
                                        <input type="text" name="phone" id="" value="{{ $item['phone'] }}" hidden>
                                        <input type="text" name="business_status" id="" value="{{ $item['business_status'] }}" hidden>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group row pt-3">
                                <div class="col-md-8">
                                    <button type="submit" name="submit" class="btn btn-primary font-weight-bold pr-5 pl-5 mr-3">
                                        {{ __('Submit') }}
                                    </button>

                                    <button type="submit" name="save" class="btn btn-outline-primary font-weight-bold pr-5 pl-5">
                                        {{ __('Save for Later') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
