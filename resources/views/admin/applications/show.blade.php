
@extends('layouts.admin')

@section('content')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
            @foreach ($applicants as $applicant)
                @foreach($compliances as $compliance)
                    @foreach($backgrounds as $background)
                        @foreach($financials as $financial)
                            @foreach($ratios as $ratio)
                                <div class="row">
                                    <div class="col-xl-12">
                                        <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
                                            <div class="m-portlet__head">
                                                <div class="m-portlet__head-tools">
                                                    <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--left m-tabs-line--primary" role="tablist">
                                                        <li class="nav-item m-tabs__item">
                                                            <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
                                                                <i class="flaticon-share m--hide"></i>
                                                                Refiner Application Details
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="tab-content">
                                                <div>
                                                    @if(Auth::user()->hasRole(0))
                                                        <div>
                                                            {{ csrf_field() }}
                                                            <div class="m-portlet__body">
                                                                <div class="row">
                                                                    <div class="col-md-2 pt-2 pb-2">
                                                                        <a href=""  onclick="window.print();" class="btn btn-primary"><span><i class="la la-print"></i></span> Print</a>
                                                                    </div>

                                                                    <div class="col-md-6 mt-3">
                                                                        <p>Abbreviations: B/S - Business,  Reg - Registration</p>
                                                                    </div>
                                                                </div><br>

                                                                <div class="form-group m-form__group row">
                                                                    <div class="col-md-10">
                                                                        <h5 class="m-form__section text-muted"><u> Applicant Information </u></h5>
                                                                    </div>
                                                                </div><br>

                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">FirstName</label>
                                                                            <p class="text-primary"> {{ $applicant->firstname }} </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">LastName</label>
                                                                            <p class="text-primary"> {{ $applicant->lastname }} </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">Race</label>
                                                                            <p class="text-primary"> {{ $applicant->race }} </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">Nationality</label>
                                                                            <p class="text-primary"> {{ $applicant->nationality }} </p>
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">Phone</label>
                                                                            <p class="text-primary"> {{ $applicant->phone }} </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">B/S Status</label>
                                                                            <p class="text-primary">
                                                                                @if($applicant->business_status == 0)
                                                                                <td data-field="Status" class="m-datatable__cell"><span style="width: 110px;">{{ "Startup" }}</span></td>
                                                                                @elseif($applicant->business_status == 1)
                                                                                <td data-field="Status" class="m-datatable__cell"><span style="width: 110px;">{{ "Existing Business" }}</span></td>
                                                                                @else
                                                                                <td></td>
                                                                                @endif
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-4">Email</label>
                                                                            <p class="text-primary"> {{ $applicant->email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </div><br>


                                                                <div class="form-group m-form__group row">
                                                                    <div class="col-md-10">
                                                                        <h5 class="m-form__section text-muted"><u> Compliance Information </u></h5>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">Company</label>
                                                                            <p class="text-primary"> {{ $compliance->company_name }} </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">Reg. No</label>
                                                                            <p class="text-primary"> {{ $compliance->registration_no }} </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">Tax Reg. No</label>
                                                                            <p class="text-primary"> {{ $compliance->tax_registration_no }} </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">VAT Reg. No</label>
                                                                            <p class="text-primary"> {{ $compliance->vat_registration_no }} </p>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">Contact Person</label>
                                                                            <p class="text-primary"> {{ $compliance->contact_person }} </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">Contact Person Email</label>
                                                                            <p class="text-primary"> {{ $compliance->contact_person_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">Contact Person Mobile</label>
                                                                            <p class="text-primary"> {{ $compliance->contact_person_mobile }} </p>
                                                                        </div>
                                                                    </div>
                                                                </div><br>

                                                                <div class="form-group m-form__group row">
                                                                    <div class="col-md-10">
                                                                        <h5 class="m-form__section text-muted"><u> Background Information </u></h5>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-2">
                                                                        <p class="font-weight-bold">Shareholder</p>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="" class="col-4"> Name</label>
                                                                            <p class="text-primary"> {{ $background->shareholder_name }} </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-4"> ID</label>
                                                                            <p class="text-primary"> {{ $background->shareholder_id }} </p>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">Entity Name</label>
                                                                            <p class="text-primary"> {{ $background->entity_name }} </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">Entity Reg. No</label>
                                                                            <p class="text-primary"> {{ $background->entity_registration_number }} </p>
                                                                        </div>
                                                                    </div>
                                                                </div><hr>

                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">Business Status</label>
                                                                            @if ($background->business_status == 0)
                                                                                <p class="text-primary"> {{ "Start Up" }} </p>
                                                                            @elseif ($background->business_status == 1)
                                                                                <p class="text-primary"> {{ "Existing Company" }} </p>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </div><hr>

                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">Individual Shareholder Percentage</label>
                                                                            <p class="text-primary"> {{ $background->individual_shareholder_percentage }}% </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">Entity Shareholder Percentage</label>
                                                                            <p class="text-primary"> {{ $background->entity_shareholder_percentage }} </p>
                                                                        </div>
                                                                    </div>
                                                                </div><hr>

                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">Management Company</label>
                                                                            <p class="text-primary"> {{ $background->management_company_name }} </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">management Reg. number</label>
                                                                            <p class="text-primary"> {{ $background->management_registration_number }} </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">management position</label>
                                                                            <p class="text-primary"> {{ $background->management_company_position }} </p>
                                                                        </div>
                                                                    </div>
                                                                </div><hr>

                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">permanent staff</label>
                                                                            <p class="text-primary"> {{ $background->permanent_staff_no }} </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">temporary staff</label>
                                                                            <p class="text-primary"> {{ $background->temporary_staff_no }} </p>
                                                                        </div>
                                                                    </div>
                                                                </div><hr>

                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">B/S location province</label>
                                                                            <p class="text-primary"> {{ $background->business_location_province }} </p>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-4">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">B/S nearest city location</label>
                                                                            <p class="text-primary"> {{ $background->business_nearest_city_location }} </p>
                                                                        </div>
                                                                    </div>
                                                                </div><hr>

                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">B/S description</label>
                                                                            <p class="text-primary"> {{ $background->business_description }} </p>
                                                                        </div>
                                                                    </div>
                                                                </div><hr>

                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">B/S goto strategy</label>
                                                                            <p class="text-primary"> {{ $background->business_goto_strategy }} </p>
                                                                        </div>
                                                                    </div>
                                                                </div><hr>

                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">B/S competitive advantage</label>
                                                                            <p class="text-primary"> {{ $background->business_competitive_advantage }} </p>
                                                                        </div>
                                                                    </div>
                                                                </div><hr>

                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">B/S areas of improvement</label>
                                                                            <p class="text-primary"> {{ $background->business_areas_of_improvement }} </p>
                                                                        </div>
                                                                    </div>
                                                                </div><hr>

                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">B/S strategic opportunities</label>
                                                                            <p class="text-primary"> {{ $background->business_strategic_opportunities }} </p>
                                                                        </div>
                                                                    </div>
                                                                </div><hr>

                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">B/S financial year end</label>
                                                                            <p class="text-primary"> {{ $background->business_financial_year_end }} </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group m-form__group row">
                                                                            <label for="example-text-input" class="col-6">B/S accountant name</label>
                                                                            <p class="text-primary"> {{ $background->business_accountant_name }} </p>
                                                                        </div>
                                                                    </div>
                                                                </div><br>


                                                                <div class="form-group m-form__group row">
                                                                    <div class="col-md-10">
                                                                        <h5 class="m-form__section text-muted"><u> Financial Information (Income Statement Overview)</u></h5>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group m-form__group">
                                                                    <div class="row">
                                                                        <table class="table table-bordered table-striped " id="table">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td></td>
                                                                                    <td class="font-weight-bold">PY -3</td>
                                                                                    <td class="font-weight-bold">PY -2</td>
                                                                                    <td class="font-weight-bold">PY -1</td>
                                                                                    <td class="font-weight-bold">YTD</td>
                                                                                    <td class="font-weight-bold">FP 1</td>
                                                                                    <td class="font-weight-bold">FP 2</td>
                                                                                    <td class="font-weight-bold">FP 3</td>
                                                                                    <td class="font-weight-bold">FP 4</td>
                                                                                    <td class="font-weight-bold">FP 5</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <label for="example-text-input" class=" font-weight-bold">Revenue</label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_revenue_py3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_revenue_py2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_revenue_py1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_revenue_ytd }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_revenue_fp1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_revenue_fp2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_revenue_fp3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_revenue_fp4 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_revenue_fp5 }} </p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <label for="example-text-input" class=" font-weight-bold">Cost of Sales</label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_cost_of_sales_py3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_cost_of_sales_py2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_cost_of_sales_py1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_cost_of_sales_ytd }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_cost_of_sales_fp1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_cost_of_sales_fp2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_cost_of_sales_fp3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_cost_of_sales_fp4 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_cost_of_sales_fp5 }} </p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <label for="example-text-input" class=" font-weight-bold">Gross Profit</label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_gross_profit_py3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_gross_profit_py2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_gross_profit_py1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_gross_profit_ytd }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_gross_profit_fp1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_gross_profit_fp2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_gross_profit_fp3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_gross_profit_fp4 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_gross_profit_fp5 }} </p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <label for="example-text-input" class=" font-weight-bold">Operating Expenses</label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_operating_expenses_py3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_operating_expenses_py2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_operating_expenses_py1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_operating_expenses_ytd }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_operating_expenses_fp1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_operating_expenses_fp2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_operating_expenses_fp3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_operating_expenses_fp4 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_operating_expenses_fp5 }} </p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <label for="example-text-input" class=" font-weight-bold">Profit Before Tax</label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_profit_before_tax_py3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_profit_before_tax_py2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_profit_before_tax_py1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_profit_before_tax_ytd }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_profit_before_tax_fp1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_profit_before_tax_fp2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_profit_before_tax_fp3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_profit_before_tax_fp4 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_profit_before_tax_fp5 }} </p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <label for="example-text-input" class=" font-weight-bold">Tax</label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_tax_py3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_tax_py2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_tax_py1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_tax_ytd }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_tax_fp1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_tax_fp2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_tax_fp3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_tax_fp4 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_tax_fp5 }} </p>
                                                                                    </td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <td>
                                                                                        <label for="example-text-input" class=" font-weight-bold">Profit After Tax</label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_profit_after_tax_py3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_profit_after_tax_py2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_profit_after_tax_py1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_profit_after_tax_ytd }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_profit_after_tax_fp1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_profit_after_tax_fp2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_profit_after_tax_fp3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_profit_after_tax_fp4 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_profit_after_tax_fp5 }} </p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <label for="example-text-input" class=" font-weight-bold">Depreciation</label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_depreciation_py3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_depreciation_py2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_depreciation_py1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_depreciation_ytd }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_depreciation_fp1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_depreciation_fp2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_depreciation_fp3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_depreciation_fp4 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_depreciation_fp5 }} </p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <label for="example-text-input" class=" font-weight-bold">Interest Expense</label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_interest_expense_py3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_interest_expense_py2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_interest_expense_py1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_interest_expense_ytd }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_interest_expense_fp1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_interest_expense_fp2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_interest_expense_fp3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_interest_expense_fp4 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->is_interest_expense_fp5 }} </p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group m-form__group row">
                                                                    <div class="col-md-10">
                                                                        <h5 class="m-form__section text-muted"><u> Financial Information (Balance Sheet Figures) </u></h5>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group m-form__group">
                                                                    <div class="row">
                                                                        <table class="table table-bordered table-striped " id="table">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td></td>
                                                                                    <td class="font-weight-bold">PY -3</td>
                                                                                    <td class="font-weight-bold">PY -2</td>
                                                                                    <td class="font-weight-bold">PY -1</td>
                                                                                    <td class="font-weight-bold">YTD</td>
                                                                                    <td class="font-weight-bold">FP 1</td>
                                                                                    <td class="font-weight-bold">FP 2</td>
                                                                                    <td class="font-weight-bold">FP 3</td>
                                                                                    <td class="font-weight-bold">FP 4</td>
                                                                                    <td class="font-weight-bold">FP 5</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <label for="example-text-input" class=" font-weight-bold">Total Assets</label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_total_assets_py3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_total_assets_py2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_total_assets_py1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_total_assets_ytd }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_total_assets_fp1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_total_assets_fp2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_total_assets_fp3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_total_assets_fp4 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_total_assets_fp5 }} </p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <label for="example-text-input" class=" font-weight-bold">Fixed Assets</label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_fixed_assets_py3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_fixed_assets_py2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_fixed_assets_py1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_fixed_assets_ytd }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_fixed_assets_fp1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_fixed_assets_fp2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_fixed_assets_fp3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_fixed_assets_fp4 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_fixed_assets_fp5 }} </p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <label for="example-text-input" class=" font-weight-bold">Current Assets</label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_current_assets_py3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_current_assets_py2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_current_assets_py1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_current_assets_ytd }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_current_assets_fp1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_current_assets_fp2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_current_assets_fp3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_current_assets_fp4 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_current_assets_fp5 }} </p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <label for="example-text-input" class=" font-weight-bold">Total Liabilities</label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_total_liabilities_py3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_total_liabilities_py2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_total_liabilities_py1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_total_liabilities_ytd }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_total_liabilities_fp1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_total_liabilities_fp2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_total_liabilities_fp3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_total_liabilities_fp4 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_total_liabilities_fp5 }} </p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <label for="example-text-input" class=" font-weight-bold">Long Term Liabilities</label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_longterm_liabilities_py3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_longterm_liabilities_py2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_longterm_liabilities_py1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_longterm_liabilities_ytd }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_longterm_liabilities_fp1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_longterm_liabilities_fp2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_longterm_liabilities_fp3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_longterm_liabilities_fp4 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_longterm_liabilities_fp5 }} </p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <label for="example-text-input" class=" font-weight-bold">Current Liabilities</label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_current_liabilities_py3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_current_liabilities_py2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_current_liabilities_py1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_current_liabilities_ytd }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_current_liabilities_fp1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_current_liabilities_fp2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_current_liabilities_fp3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_current_liabilities_fp4 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_current_liabilities_fp5 }} </p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <label for="example-text-input" class=" font-weight-bold">Equity</label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_equity_py3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_equity_py2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_equity_py1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_equity_ytd }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_equity_fp1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_equity_fp2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_equity_fp3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_equity_fp4 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_equity_fp5 }} </p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <label for="example-text-input" class=" font-weight-bold">Shareholder Loans</label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_shareholder_loans_py3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_shareholder_loans_py2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_shareholder_loans_py1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_shareholder_loans_ytd }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_shareholder_loans_fp1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_shareholder_loans_fp2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_shareholder_loans_fp3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_shareholder_loans_fp4 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_shareholder_loans_fp5 }} </p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <label for="example-text-input" class=" font-weight-bold">Capital Expenditure</label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_capital_expenditure_py3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_capital_expenditure_py2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_capital_expenditure_py1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_capital_expenditure_ytd }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_capital_expenditure_fp1 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_capital_expenditure_fp2 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_capital_expenditure_fp3 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_capital_expenditure_fp4 }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $financial->bs_capital_expenditure_fp5 }} </p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group m-form__group row">
                                                                    <div class="col-md-10">
                                                                        <h5 class="m-form__section text-muted"><u> Key Ratios </u></h5>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group m-form__group">
                                                                    <div class="row">
                                                                        <table class="table table-bordered table-striped " id="table">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td></td>
                                                                                    <td class="font-weight-bold">PY -3</td>
                                                                                    <td class="font-weight-bold">PY -2</td>
                                                                                    <td class="font-weight-bold">PY -1</td>
                                                                                    <td class="font-weight-bold">YTD</td>
                                                                                    <td class="font-weight-bold">FP 1</td>
                                                                                    <td class="font-weight-bold">FP 2</td>
                                                                                    <td class="font-weight-bold">FP 3</td>
                                                                                    <td class="font-weight-bold">FP 4</td>
                                                                                    <td class="font-weight-bold">FP 5</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <label for="example-text-input" class=" font-weight-bold">Revenue Growth (%)</label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->py3_revenue_growth }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->py2_revenue_growth }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->py1_revenue_growth }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->ytd_revenue_growth }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp1_revenue_growth }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp2_revenue_growth }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp3_revenue_growth }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp4_revenue_growth }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp5_revenue_growth }} </p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <label for="example-text-input" class=" font-weight-bold">Gross Profit Margin (%)</label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->py3_gp_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->py2_gp_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->py1_gp_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->ytd_gp_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp1_gp_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp2_gp_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp3_gp_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp4_gp_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp5_gp_margin }} </p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <label for="example-text-input" class=" font-weight-bold">PBT Margin (%)</label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->py3_pbt_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->py2_pbt_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->py1_pbt_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->ytd_pbt_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp1_pbt_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp2_pbt_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp3_pbt_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp4_pbt_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp5_pbt_margin }} </p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <label for="example-text-input" class=" font-weight-bold">EBITDA</label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->py3_ebitda }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->py2_ebitda }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->py1_ebitda }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->ytd_ebitda }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp1_ebitda }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp2_ebitda }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp3_ebitda }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp4_ebitda }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp5_ebitda }} </p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <label for="example-text-input" class=" font-weight-bold">EBITDA Margin (%)</label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->py3_ebitda_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->py2_ebitda_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->py1_ebitda_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->ytd_ebitda_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp1_ebitda_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp2_ebitda_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp3_ebitda_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp4_ebitda_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp5_ebitda_margin }} </p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <label for="example-text-input" class=" font-weight-bold">PAT Margin (%)</label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->py3_pat_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->py2_pat_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->py1_pat_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->ytd_pat_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp1_pat_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp2_pat_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp3_pat_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp4_pat_margin }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp5_pat_margin }} </p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <label for="example-text-input" class=" font-weight-bold">TNAV</label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->py3_tnav }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->py2_tnav }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->py1_tnav }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->ytd_tnav }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp1_tnav }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp2_tnav }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp3_tnav }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp4_tnav }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp5_tnav }} </p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <label for="example-text-input" class=" font-weight-bold">Current Ratio</label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->py3_current_ratio }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->py2_current_ratio }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->py1_current_ratio }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->ytd_current_ratio }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp1_current_ratio }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp2_current_ratio }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp3_current_ratio }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp4_current_ratio }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp5_current_ratio }} </p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <label for="example-text-input" class=" font-weight-bold">Debt Ratio</label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->py3_debt_ratio }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->py2_debt_ratio }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->py1_debt_ratio }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->ytd_debt_ratio }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp1_debt_ratio }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp2_debt_ratio }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp3_debt_ratio }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp4_debt_ratio }} </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <p class="text-primary"> {{ $ratio->fp5_debt_ratio }} </p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @else

                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endforeach
                    @endforeach
                @endforeach
            @endforeach
        <!--End::Section-->
        </div>
    </div>
</div>
<!-- end:: Body -->

@endsection

































