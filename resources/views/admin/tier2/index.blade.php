@extends('layouts.admin')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Tier 2 Applicants
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            @if(Auth::user()->hasRole(0))
                                {{-- <li class="m-portlet__nav-item">
                                    <a href="{{ url('new-vehicle') }}"
                                        class="btn btn-primary m-btn  m-btn--custom m-btn--icon m-btn--air">
                                        <span>
                                            <i class="fa fa-edit"></i>
                                            <span>New Application</span>
                                        </span>
                                    </a>
                                </li> --}}
                            @endif
                            <li class="m-portlet__nav-item"></li>
                        </ul>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <table class="table table-bordered table-striped " id="table">
                        <thead class>
                            @if(Auth::user()->hasRole(0))
                                <tr>
                                    <th>Firstname</th>
                                    <th>Lastname</th>
                                    <th>Race</th>
                                    <th>Nationality</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Business Status</th>
                                    <th>Action</th>
                                </tr>
                            @elseif(Auth::user()->hasRole(1))
                                <tr>
                                    <th>Firstname</th>
                                    <th>Lastname</th>
                                    <th>Race</th>
                                    <th>Nationality</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Business Status</th>
                                    <th>Action</th>
                                </tr>
                            @else

                            @endif
                        </thead>
                        <tbody>
                            @if(Auth::user()->hasRole(0))
                                @foreach ($ids as $id)
                                    @foreach($applicants as $applicant)
                                        @if ($applicant->id == $id->id)
                                            <tr>
                                                <td>{{ $applicant->firstname }}</td>
                                                <td>{{ $applicant->lastname }}</td>
                                                <td>{{ $applicant->race }}</td>
                                                <td>{{ $applicant->nationality }}</td>
                                                <td>{{ $applicant->email }}</td>
                                                <td>{{ $applicant->phone }}</td>
                                                @if($applicant->business_status == 0)
                                                    <td data-field="Status" class="m-datatable__cell"><span style="width: 110px;">{{ "Startup" }}</span></td>
                                                @elseif($applicant->business_status == 1)
                                                    <td data-field="Status" class="m-datatable__cell"><span style="width: 110px;">{{ "Existing Business" }}</span></td>
                                                @else
                                                    <td></td>
                                                @endif

                                                <td>
                                                    <a href="{{ url('show-application/'.$applicant->id) }}"
                                                        class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                                                        title="View ">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @endforeach
                            @elseif(Auth::user()->hasRole(1))
                                @foreach ($ids as $id)
                                    @foreach($applicants as $applicant)
                                        @if ($applicant->id == $id->id)
                                            <tr>
                                                <td>{{ $applicant->firstname }}</td>
                                                <td>{{ $applicant->lastname }}</td>
                                                <td>{{ $applicant->race }}</td>
                                                <td>{{ $applicant->nationality }}</td>
                                                <td>{{ $applicant->email }}</td>
                                                <td>{{ $applicant->phone }}</td>
                                                @if($applicant->business_status == 0)
                                                    <td data-field="Status" class="m-datatable__cell"><span style="width: 110px;">{{ "Startup" }}</span></td>
                                                @elseif($applicant->business_status == 1)
                                                    <td data-field="Status" class="m-datatable__cell"><span style="width: 110px;">{{ "Existing Business" }}</span></td>
                                                @else
                                                    <td></td>
                                                @endif

                                                <td>
                                                    <a href="{{ url('show-application/'.$applicant->id) }}"
                                                        class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                                                        title="View ">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    </div>
    <!-- end:: Body -->
@endsection
