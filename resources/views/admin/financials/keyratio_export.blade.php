
<table>
    @foreach ($ratios as $ratio)
        <tbody>
            <tr>
                <td></td>
                <td>Tier ({{ ($ratio->tier) }})</td>
                <td>PY -3</td>
                <td>PY -2</td>
                <td>PY -1</td>
                <td>YTD</td>
                <td>FP 1</td>
                <td>FP 2</td>
                <td>FP 3</td>
                <td>FP 4</td>
                <td>FP 5</td>
            </tr>
            <tr>
                <td>
                    <label for="example-text-input" class=" font-weight-bold">Revenue Growth (%)</label>
                </td>
                <td></td>
                <td>
                    <h5> {{ $ratio->py3_revenue_growth }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->py2_revenue_growth }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->py1_revenue_growth }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->ytd_revenue_growth }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp1_revenue_growth }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp2_revenue_growth }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp3_revenue_growth }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp4_revenue_growth }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp5_revenue_growth }} </h5>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="example-text-input" class=" font-weight-bold">Gross Profit Margin (%)</label>
                </td>
                <td></td>
                <td>
                    <h5> {{ $ratio->py3_gp_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->py2_gp_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->py1_gp_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->ytd_gp_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp1_gp_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp2_gp_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp3_gp_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp4_gp_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp5_gp_margin }} </h5>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="example-text-input" class=" font-weight-bold">PBT Margin (%)</label>
                </td>
                <td></td>
                <td>
                    <h5> {{ $ratio->py3_pbt_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->py2_pbt_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->py1_pbt_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->ytd_pbt_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp1_pbt_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp2_pbt_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp3_pbt_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp4_pbt_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp5_pbt_margin }} </h5>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="example-text-input" class=" font-weight-bold">EBITDA</label>
                </td>
                <td></td>
                <td>
                    <h5> {{ $ratio->py3_ebitda }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->py2_ebitda }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->py1_ebitda }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->ytd_ebitda }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp1_ebitda }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp2_ebitda }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp3_ebitda }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp4_ebitda }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp5_ebitda }} </h5>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="example-text-input" class=" font-weight-bold">EBITDA Margin (%)</label>
                </td>
                <td></td>
                <td>
                    <h5> {{ $ratio->py3_ebitda_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->py2_ebitda_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->py1_ebitda_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->ytd_ebitda_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp1_ebitda_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp2_ebitda_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp3_ebitda_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp4_ebitda_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp5_ebitda_margin }} </h5>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="example-text-input" class=" font-weight-bold">PAT Margin (%)</label>
                </td>
                <td></td>
                <td>
                    <h5> {{ $ratio->py3_pat_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->py2_pat_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->py1_pat_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->ytd_pat_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp1_pat_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp2_pat_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp3_pat_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp4_pat_margin }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp5_pat_margin }} </h5>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="example-text-input" class=" font-weight-bold">TNAV</label>
                </td>
                <td></td>
                <td>
                    <h5> {{ $ratio->py3_tnav }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->py2_tnav }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->py1_tnav }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->ytd_tnav }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp1_tnav }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp2_tnav }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp3_tnav }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp4_tnav }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp5_tnav }} </h5>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="example-text-input" class=" font-weight-bold">Current Ratio</label>
                </td>
                <td></td>
                <td>
                    <h5> {{ $ratio->py3_current_ratio }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->py2_current_ratio }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->py1_current_ratio }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->ytd_current_ratio }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp1_current_ratio }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp2_current_ratio }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp3_current_ratio }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp4_current_ratio }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp5_current_ratio }} </h5>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="example-text-input" class=" font-weight-bold">Debt Ratio</label>
                </td>
                <td></td>
                <td>
                    <h5> {{ $ratio->py3_debt_ratio }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->py2_debt_ratio }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->py1_debt_ratio }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->ytd_debt_ratio }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp1_debt_ratio }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp2_debt_ratio }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp3_debt_ratio }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp4_debt_ratio }} </h5>
                </td>
                <td>
                    <h5> {{ $ratio->fp5_debt_ratio }} </h5>
                </td>
            </tr>
        </tbody>
    @endforeach
</table>
































